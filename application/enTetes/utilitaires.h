#ifndef H_UTILITAIRES
#define H_UTILITAIRES

#include "arbres.h"

// structure contenant toutes les infos sur la latitude pour le filtre
typedef struct filtreLatitude{
	int actif;
	float latitudeMin;
	float latitudeMax;
} FILTRELATITUDE;

// structure contenant toutes les infos sur la longitude pour le filtre
typedef struct filtreLongitude{
	int actif;
	float longitudeMin;
	float longitudeMax;
} FILTRELONGITUDE;

// structure contenant toutes les infos sur la date pour le filtre
typedef struct filtreDate{
	int actif;
	DATE dateMin;
	DATE dateMax;
} FILTREDATE;

// structure filtre permettant de stocker les infos nécessaire pour filtrer les données
typedef struct filtre{
	FILTRELATITUDE latitude;
	FILTRELONGITUDE longitude;
	FILTREDATE date;
} FILTRE;

/*
	Auteur : Dorian BARRERE
	Date : 27-12-2022
	Résumer : récupére les données pour le générateur TM1
	Entrée : le pointeur vers le fichier csv ouvert, les adresses des variables contenant l'id de station/la date de la mesure (année/mois/jour)/les coo (latitude/longitude)/la température
	Sortie : 1 si ok, 0 sinon
*/
int recupererTM1(FILE* fichierCSV, long int* idStation, int* annee, int* mois, int* jour, double* latitude, double* longitude, double* temperature);

/*
	Auteur : Dorian BARRERE
	Date : 28-12-2022
	Résumer : récupére les données pour le générateur TM2
	Entrée : le pointeur vers le fichier csv ouvert, les adresses des variables contenant la date de la mesure (année/mois/jour/heure)/les coo (latitude/longitude)/la température
	Sortie : 1 si ok, 0 sinon
*/
int recupererTM2(FILE* fichierCSV, int* annee, int* mois, int* jour, int* heure, double* latitude, double* longitude, double* temperature);

/*
	Auteur : Dorian BARRERE
	Date : 28-12-2022
	Résumer : récupére les données pour le générateur TM3
	Entrée : le pointeur vers le fichier csv ouvert, les adresses des variables contenant l'id de station/la date de la mesure (année/mois/jour/heure)/les coo (latitude/longitude)/la température
	Sortie : 1 si ok, 0 sinon
*/
int recupererTM3(FILE* fichierCSV, long int* idStation, int* annee, int* mois, int* jour, int* heure, double* latitude, double* longitude, double* temperature);

/*
	Auteur : Dorian BARRERE
	Date : 29-12-2022
	Résumer : récupére les données pour le générateur PM1
	Entrée : le pointeur vers le fichier csv ouvert, les adresses des variables contenant l'id de station/la date de la mesure (année/mois/jour)/les coo (latitude/longitude)/la pression
	Sortie : 1 si ok, 0 sinon
*/
int recupererPM1(FILE* fichierCSV, long int* idStation, int* annee, int* mois, int* jour, double* latitude, double* longitude, double* pression);

/*
	Auteur : Dorian BARRERE
	Date : 29-12-2022
	Résumer : récupére les données pour le générateur PM2
	Entrée : le pointeur vers le fichier csv ouvert, les adresses des variables contenant la date de la mesure (année/mois/jour/heure)/les coo (latitude/longitude)/la pression
	Sortie : 1 si ok, 0 sinon
*/
int recupererPM2(FILE* fichierCSV, int* annee, int* mois, int* jour, int* heure, double* latitude, double* longitude, double* pression);

/*
	Auteur : Dorian BARRERE
	Date : 29-12-2022
	Résumer : récupére les données pour le générateur PM3
	Entrée : le pointeur vers le fichier csv ouvert, les adresses des variables contenant l'id de station/la date de la mesure (année/mois/jour/heure)/les coo (latitude/longitude)/la pression
	Sortie : 1 si ok, 0 sinon
*/
int recupererPM3(FILE* fichierCSV, long int* idStation, int* annee, int* mois, int* jour, int* heure, double* latitude, double* longitude, double* pression);

/*
	Auteur : Dorian BARRERE
	Date : 30-12-2022
	Résumer : récupére les données pour le générateur Vent
	Entrée : le pointeur vers le fichier csv ouvert, les adresses des variables contenant l'id de station/la date de la mesure (année/mois/jour)/la direction du vent/la vitesse du vent/les coo (latitude/longitude)
	Sortie : 1 si ok, 0 sinon
*/
int recupererVent(FILE* fichierCSV, long int* idStation, int* annee, int* mois, int* jour, double* directionVent, double* vitesseVent, double* latitude, double* longitude);

/*
	Auteur : Dorian BARRERE
	Date : 31-12-2022
	Résumer : récupére les données pour le générateur Hauteur
	Entrée : le pointeur vers le fichier csv ouvert, les adresses des variables contenant la date de la mesure (année/mois/jour)/les coo (latitude/longitude)/l'altitude
	Sortie : 1 si ok, 0 sinon
*/
int recupererHauteur(FILE* fichierCSV, int* annee, int* mois, int* jour, double* latitude, double* longitude, double* altitude);

/*
	Auteur : Dorian BARRERE
	Date : 31-12-2022
	Résumer : récupére les données pour le générateur Humidité
	Entrée : le pointeur vers le fichier csv ouvert, les adresses des variables contenant l'id de station/la date de la mesure (année/mois/jour)/l'humidité/les coo (latitude/longitude)
	Sortie : 1 si ok, 0 sinon
*/
int recupererHumidite(FILE* fichierCSV, long int* idStation, int* annee, int* mois, int* jour, float* humidite, double* latitude, double* longitude);

/*
	Auteur : Dorian BARRERE
	Date : 27-12-2022
	Résumer : écrit dans un fichier toutes les données que contient un arbre (parcours infixe) pour le générateur TM1
	Entrée : le pointeur vers le fichier csv ouvert, l'arbre
	Sortie : 1 si ok, 0 sinon
*/
int ecrireTM1(FILE* fichierCSV, ARBRE a);

/*
	Auteur : Dorian BARRERE
	Date : 28-12-2022
	Résumer : écrit dans un fichier toutes les données que contient un arbre (parcours infixe) pour le générateur TM2
	Entrée : le pointeur vers le fichier csv ouvert, l'arbre
	Sortie : 1 si ok, 0 sinon
*/
int ecrireTM2(FILE* fichierCSV, ARBRE a);

/*
	Auteur : Dorian BARRERE
	Date : 28-12-2022
	Résumer : écrit dans un fichier toutes les données que contient un arbre (parcours infixe) pour le générateur TM3
	Entrée : le pointeur vers le fichier csv ouvert, l'arbre
	Sortie : 1 si ok, 0 sinon
*/
int ecrireTM3(FILE* fichierCSV, ARBRE a);

/*
	Auteur : Dorian BARRERE
	Date : 29-12-2022
	Résumer : écrit dans un fichier toutes les données que contient un arbre (parcours infixe) pour le générateur PM1
	Entrée : le pointeur vers le fichier csv ouvert, l'arbre
	Sortie : 1 si ok, 0 sinon
*/
int ecrirePM1(FILE* fichierCSV, ARBRE a);

/*
	Auteur : Dorian BARRERE
	Date : 29-12-2022
	Résumer : écrit dans un fichier toutes les données que contient un arbre (parcours infixe) pour le générateur PM2
	Entrée : le pointeur vers le fichier csv ouvert, l'arbre
	Sortie : 1 si ok, 0 sinon
*/
int ecrirePM2(FILE* fichierCSV, ARBRE a);

/*
	Auteur : Dorian BARRERE
	Date : 29-12-2022
	Résumer : écrit dans un fichier toutes les données que contient un arbre (parcours infixe) pour le générateur PM3
	Entrée : le pointeur vers le fichier csv ouvert, l'arbre
	Sortie : 1 si ok, 0 sinon
*/
int ecrirePM3(FILE* fichierCSV, ARBRE a);

/*
	Auteur : Dorian BARRERE
	Date : 30-12-2022
	Résumer : écrit dans un fichier toutes les données que contient un arbre (parcours infixe) pour le générateur Vent
	Entrée : le pointeur vers le fichier csv ouvert, l'arbre
	Sortie : 1 si ok, 0 sinon
*/
int ecrireVent(FILE* fichierCSV, ARBRE a);

/*
	Auteur : Dorian BARRERE
	Date : 31-12-2022
	Résumer : écrit dans un fichier toutes les données que contient un arbre (parcours infixe) pour le générateur Hauteur
	Entrée : le pointeur vers le fichier csv ouvert, l'arbre
	Sortie : 1 si ok, 0 sinon
*/
int ecrireHauteur(FILE* fichierCSV, ARBRE a);

/*
	Auteur : Dorian BARRERE
	Date : 31-12-2022
	Résumer : écrit dans un fichier toutes les données que contient un arbre (parcours infixe) pour le générateur Humidité
	Entrée : le pointeur vers le fichier csv ouvert, l'arbre
	Sortie : 1 si ok, 0 sinon
*/
int ecrireHumidite(FILE* fichierCSV, ARBRE a);

/*
	Auteur : Dorian BARRERE
	Date : 27-12-2022
	Résumer : permet de parcourir une ligne pour aller à la suivante
	Entrée : le pointeur vers le fichier csv ouvert
	Sortie : rien
*/
void ligneSuitante(FILE* fichierCSV);

/*
	Auteur : Dorian BARRERE
	Date : 28-12-2022
	Résumer : vérifie si 2 dates sont équivalentes
	Entrée : les 2 dates
	Sortie : 1 si ok, 0 sinon
*/
int estMemeDate(DATE date1, DATE date2);

/*
	Auteur : Dorian BARRERE
	Date : 28-12-2022
	Résumer : vérifie si la première date est après la seconde
	Entrée : les 2 dates
	Sortie : 1 si ok, 0 sinon
*/
int estApres(DATE date1, DATE date2);

/*
	Auteur : Dorian BARRERE
	Date : 30-12-2022
	Résumer : transforme un angle en degré [0;360] en un anglen radian ]-pi;pi]
	Entrée : l'angle entre 0 et 360 degrés
	Sortie : 1 si ok, 0 sinon
*/
double degre360EnRadianSymetrique(double angleDegre360);

/*
	Auteur : Dorian BARRERE
	Date : 02-01-2023
	Résumer : vérifie si un donnée appartient au filtre
	Entrée : le filtre, la latitude, la longitude, l'année, le mois et le jour
	Sortie : 1 si ok, 0 sinon
*/
int appartientAuFiltre(FILTRE filtre, double latitude, double longitude, int annee, int mois, int jour);

/*
	Auteur : Dorian BARRERE
	Date : 05-01-2023
	Résumer : initialise tout le filtre à 0
	Entrée : rien
	Sortie : le filter initialisé
*/
FILTRE* initialiserFiltre();

/* 
	Auteur : Julien Gitton
	Date : 05/01/2023
	Résumé : Permet de vérifier qu'une date est valide
	Entrée(s) : Une date
	Sorties(s) : 1 si la date est valide, 0 sinon
*/
int estValide(DATE date);

/*
	Auteur : Dorian BARRERE
	Date : 14-01-2023
	Résumer : vérifie si un chemin spécifié est un dossier ou non
	Entrée : le chemin sous forme de chaîne de caractères
	Sortie : 1 si c'est un dossier (répertoire), 0 sinon
*/
int estDossier(char* chemin);

#endif