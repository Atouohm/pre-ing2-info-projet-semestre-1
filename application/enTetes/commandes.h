#ifndef H_COMMANDES
#define H_COMMANDES

#include "arbres.h"

// la structure représentant la liste des instructions
typedef struct listeInstructions{
	int aideDrapeau; // les drapeaux signifient si l'instruction est à faire (1), ou non (0)
	int tDrapeau;
	int tValeur; // les valeurs sont les arguments des options et du drapeau associé
	int pDrapeau;
	int pValeur;
	int wDrapeau;
	int hDrapeau;
	int mDrapeau;
	int gDrapeau;
	double gValeurMin;
	double gValeurMax;
	int aDrapeau;
	double aValeurMin;
	double aValeurMax;
	int dDrapeau;
	DATE dValeurMin;
	DATE dValeurMax;
	int iDrapeau;
	char* iValeur;
	int oDrapeau;
	char* oValeur;
} LISTEINSTRUCTIONS;

/*
	Auteur : Tara FAZILLE
	Date : 14-12-2022
	Résumer : affiche l'aide
	Entrée : rien
	Sortie : rien
*/
void aideApplication();

/*
	Auteur : Dorian BARRERE
	Date : 04-01-2023
	Résumer : fonction qui récupérer les instructions et analysant la commande
	Entrée : le arc et le argv du main, un pointeur (de pointeur vers une liste d'instruction) NULL (il sera alloué et initialisé automatiquement dans la fonction)
	Sortie : 1 si la commande était correcte, 0 sinon
*/
int recupererInstructions(const int argc, const char* const argv[], LISTEINSTRUCTIONS** listeInstructions);

#endif
