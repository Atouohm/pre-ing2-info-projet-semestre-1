#ifndef H_GENERATEURS
#define H_GENERATEURS

#include "utilitaires.h"

/*
	Auteur : Dorian BARRERE
	Date : 27-12-2022
	Résumer : rassemble les générateurs des fichiers de données Température Mode 1, 2 et 3
	Entrée : le mode désiré (1/2/3), le filtre des données, l'adresse du fichier d'entrée ainsi que celui de sortie sous forme de chaîne de caractères
	Sortie : 0 si ok, 1 si erreur
*/
int temperature(int mode, FILTRE filtre, char* fichierEntree, char* fichierSortie);

/*
	Auteur : Dorian BARRERE
	Date : 29-12-2022
	Résumer : rassemble les générateurs des fichiers de données Pression Mode 1, 2 et 3
	Entrée : le mode désiré (1/2/3), le filtre des données, l'adresse du fichier d'entrée ainsi que celui de sortie sous forme de chaîne de caractères
	Sortie : 0 si ok, 1 si erreur
*/
int pression(int mode, FILTRE filtre, char* fichierEntree, char* fichierSortie);

/*
	Auteur : Dorian BARRERE
	Date : 30-12-2022
	Résumer : génère un fichier de données Vent
	Entrée : le filtre des données, l'adresse du fichier d'entrée ainsi que celui de sortie sous forme de chaîne de caractères
	Sortie : 0 si ok, 1 si erreur
*/
int vent(FILTRE filtre, char* fichierEntree, char* fichierSortie);

/*
	Auteur : Dorian BARRERE
	Date : 31-12-2022
	Résumer : génère un fichier de données Hauteur
	Entrée : le filtre des données, l'adresse du fichier d'entrée ainsi que celui de sortie sous forme de chaîne de caractères
	Sortie : 0 si ok, 1 si erreur
*/
int hauteur(FILTRE filtre, char* fichierEntree, char* fichierSortie);


/*
	Auteur : Dorian BARRERE
	Date : 31-12-2022
	Résumer : génère un fichier de données Humidité
	Entrée : le filtre des données, l'adresse du fichier d'entrée ainsi que celui de sortie sous forme de chaîne de caractères
	Sortie : 0 si ok, 1 si erreur
*/
int humidite(FILTRE filtre, char* fichierEntree, char* fichierSortie);

#endif