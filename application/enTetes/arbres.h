#ifndef H_ARBRES
#define H_ARBRES

// structure représentant les coordonnées
typedef struct coo{
	double latitude;
	double longitude;
	double altitude;
} COO;

// structure représentant la date
typedef struct date{
	int annee;
	int mois;
	int jour;
	int heure;
} DATE;

// structure contenant toutes les infos sur la température
typedef struct temperature{
	double sommeTemperatures;
	long int nombreMesures;
	double temperatureMin;
	double temperatureMax;
} TEMPERATURE;

// structure contenant toutes les infos sur la pression
typedef struct pression{
	double sommePressions;
	long int nombreMesures;
	double pressionMin;
	double pressionMax;
} PRESSION;

// structure contenant toutes les infos sur le vent
typedef struct vent{
	double sommeComposantesX;
	double sommeComposantesY;
	int nombreMesures;
} VENT;

// structure recrésentant les données d'un noeud. Les structures imbriquées COO/DATE... sont en pointeur pour pouvoir ou non les allouer et ainsi gagner de l'espace
typedef struct donnee{
	long int idStation;
	float humiditeMax;
	COO* coordonnees;
	DATE* date;
	TEMPERATURE* temperature;
	PRESSION* pression;
	VENT* vent;
} DONNEE;

// structure représentant un noeud d'arbre
typedef struct noeud{
	int hauteur;
	DONNEE* donnee;
	struct noeud* filsG;
	struct noeud* filsD;
} NOEUD;

// structure représentant un arbre
typedef NOEUD* ARBRE;

/*
	Auteur : Dorian BARRERE
	Date : 27-12-2022
	Résumer : insère une donnée dans un arbre pour le générateur TM1
	Entrée : le pointeur de la donnée, l'arbre
	Sortie : l'arbre contenant la nouvelle donnée
*/
ARBRE insertionTM1(DONNEE* donnee, ARBRE a);

/*
	Auteur : Dorian BARRERE
	Date : 28-12-2022
	Résumer : insère une donnée dans un arbre pour le générateur TM2
	Entrée : le pointeur de la donnée, l'arbre
	Sortie : l'arbre contenant la nouvelle donnée
*/
ARBRE insertionTM2(DONNEE* donnee, ARBRE a);

/*
	Auteur : Dorian BARRERE
	Date : 28-12-2022
	Résumer : insère une donnée dans un arbre pour le générateur TM3
	Entrée : le pointeur de la donnée, l'arbre
	Sortie : l'arbre contenant la nouvelle donnée
*/
ARBRE insertionTM3(DONNEE* donnee, ARBRE a);

/*
	Auteur : Dorian BARRERE
	Date : 29-12-2022
	Résumer : insère une donnée dans un arbre pour le générateur PM1
	Entrée : le pointeur de la donnée, l'arbre
	Sortie : l'arbre contenant la nouvelle donnée
*/
ARBRE insertionPM1(DONNEE* donnee, ARBRE a);

/*
	Auteur : Dorian BARRERE
	Date : 29-12-2022
	Résumer : insère une donnée dans un arbre pour le générateur PM2
	Entrée : le pointeur de la donnée, l'arbre
	Sortie : l'arbre contenant la nouvelle donnée
*/
ARBRE insertionPM2(DONNEE* donnee, ARBRE a);

/*
	Auteur : Dorian BARRERE
	Date : 29-12-2022
	Résumer : insère une donnée dans un arbre pour le générateur PM3
	Entrée : le pointeur de la donnée, l'arbre
	Sortie : l'arbre contenant la nouvelle donnée
*/
ARBRE insertionPM3(DONNEE* donnee, ARBRE a);

/*
	Auteur : Dorian BARRERE
	Date : 30-12-2022
	Résumer : insère une donnée dans un arbre pour le générateur Vent
	Entrée : le pointeur de la donnée, l'arbre
	Sortie : l'arbre contenant la nouvelle donnée
*/
ARBRE insertionVent(DONNEE* donnee, ARBRE a);

/*
	Auteur : Dorian BARRERE
	Date : 31-12-2022
	Résumer : vérifie si une station est déjà présente (en regardant les coordonnées) dans un arbre déjà trié (selon l'altitude décroissante des stations) grâce à l'altitude de la station
	Entrée : l'arbre, la longitude, la latitude, l'altitude
	Sortie : 1 si vrai, 0 si faux
*/
int stationDejaPresente(ARBRE a, double latitude, double longitude, double altitude);

/*
	Auteur : Dorian BARRERE
	Date : 31-12-2022
	Résumer : insère une donnée dans un arbre pour le générateur Hauteur
	Entrée : le pointeur de la donnée, l'arbre
	Sortie : l'arbre contenant la nouvelle donnée
*/
ARBRE insertionHauteur(DONNEE* donnee, ARBRE a);

/*
	Auteur : Dorian BARRERE
	Date : 31-12-2022
	Résumer : insère une donnée dans un arbre pour le générateur Humidité mais par ids des stations décroissantes
	Entrée : le pointeur de la donnée, l'arbre
	Sortie : l'arbre contenant la nouvelle donnée
*/
ARBRE insertionHumidite(DONNEE* donnee, ARBRE a);

/*
	Auteur : Dorian BARRERE
	Date : 31-12-2022
	Résumer : trie un arbre par humiditées décroissantes
	Entrée : le pointeur vers l'arbre
	Sortie : rien
*/
void trierHumidite(ARBRE* arbreNonTrie);

/*
	Auteur : Dorian BARRERE
	Date : 27-12-2022
	Résumer : vide un arbre qui a été utilisé par le générateur TM1
	Entrée : l'arbre
	Sortie : l'arbre vidé (donc NULL)
*/
ARBRE viderArbreTM1(ARBRE a);

/*
	Auteur : Dorian BARRERE
	Date : 28-12-2022
	Résumer : vide un arbre qui a été utilisé par le générateur TM2
	Entrée : l'arbre
	Sortie : l'arbre vidé (donc NULL)
*/
ARBRE viderArbreTM2(ARBRE a);

/*
	Auteur : Dorian BARRERE
	Date : 28-12-2022
	Résumer : vide un arbre qui a été utilisé par le générateur TM3
	Entrée : l'arbre
	Sortie : l'arbre vidé (donc NULL)
*/
ARBRE viderArbreTM3(ARBRE a);

/*
	Auteur : Dorian BARRERE
	Date : 29-12-2022
	Résumer : vide un arbre qui a été utilisé par le générateur PM1
	Entrée : l'arbre
	Sortie : l'arbre vidé (donc NULL)
*/
ARBRE viderArbrePM1(ARBRE a);

/*
	Auteur : Dorian BARRERE
	Date : 29-12-2022
	Résumer : vide un arbre qui a été utilisé par le générateur PM2
	Entrée : l'arbre
	Sortie : l'arbre vidé (donc NULL)
*/
ARBRE viderArbrePM2(ARBRE a);

/*
	Auteur : Dorian BARRERE
	Date : 29-12-2022
	Résumer : vide un arbre qui a été utilisé par le générateur PM3
	Entrée : l'arbre
	Sortie : l'arbre vidé (donc NULL)
*/
ARBRE viderArbrePM3(ARBRE a);

/*
	Auteur : Dorian BARRERE
	Date : 30-12-2022
	Résumer : vide un arbre qui a été utilisé par le générateur Vent
	Entrée : l'arbre
	Sortie : l'arbre vidé (donc NULL)
*/
ARBRE viderArbreVent(ARBRE a);

/*
	Auteur : Dorian BARRERE
	Date : 31-12-2022
	Résumer : vide un arbre qui a été utilisé par le générateur Hauteur
	Entrée : l'arbre
	Sortie : l'arbre vidé (donc NULL)
*/
ARBRE viderArbreHauteur(ARBRE a);

/*
	Auteur : Dorian BARRERE
	Date : 31-12-2022
	Résumer : vide un arbre qui a été utilisé par le générateur Humidité
	Entrée : l'arbre
	Sortie : l'arbre vidé (donc NULL)
*/
ARBRE viderArbreHumidite(ARBRE a);

#endif