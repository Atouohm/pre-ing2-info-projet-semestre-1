/* on inclu tout ce dont à a besoin */
#include "librairiesBasiques.h"
#include "arbres.h"
#include "utilitaires.h"
#include "generateurs.h"

/*
	Auteur : Dorian BARRERE
	Date : 27-12-2022
	Résumer : générateur TM1
	Entrée : le filtre, les 2 chaînes pour le fichier "entrée" et le fichier "sortie"
	Sortie : 0 si ok, 1 si pas bon, 3 si pas bon mais par une erreur interne qui ne devraient pas arriver
*/
int temperatureMode1(FILTRE filtre, char* fichierEntree, char* fichierSortie){
	/* on déclare et initialise les variables nécessaires pour la suite */
	ARBRE arbre = NULL; // on déclare et initialise à NULL un arbre
	DONNEE* donnee; // on déclare un pointeur de donnée
	long int idStation; // l'id de station en entier long
	double temperature, latitude, longitude; // la température, la latitude et la longitude en type double
	int annee, mois, jour, retour; // l'année, le mois, le jour et la variable retour en tant qu'entiers

	if(estDossier(fichierEntree)){ // avant d'essayer d'ouvrir le fichier on vérifie que s'en est bien un
		printf("\n-!- erreur : le fichier des données n'a pas pu être ouvert, vous avez spécifié un dossier (répertoire) -!-\n");
		return EXIT_FAILURE;
	}

	FILE* fichierCSV = fopen(fichierEntree, "r"); // on ouvre en lecture le fichier d'entrée
	if(fichierCSV==NULL){ // si on a pas réussi alors erreur et on termine la fonction
		printf("\n-!- erreur : le fichier des données n'a pas pu être ouvert -!-\n");
		return EXIT_FAILURE;
	}

	fscanf(fichierCSV, "%*[^\n]\n"); // on passe la première ligne du csv qui est la description des colonnes
	while(!feof(fichierCSV)){ // tant qu'on est pas à la fin du fichier
		if(recupererTM1(fichierCSV, &idStation, &annee, &mois, &jour, &latitude, &longitude, &temperature) && appartientAuFiltre(filtre, latitude, longitude, annee, mois, jour)){ // on récupère les données de la ligne (et on vérifie qu'elles sont disponibles) et on vérifie qu'elles appartiennent au filtre
			/* dans ce cas on créé une variable donnée qui stockera les données récupérées */
			donnee = malloc(sizeof(DONNEE));
			donnee->idStation = idStation;
			donnee->temperature = malloc(sizeof(TEMPERATURE)); // on alloue ce dont on a besoin
			donnee->temperature->sommeTemperatures = temperature;
			donnee->temperature->nombreMesures = 1;
			donnee->temperature->temperatureMax = temperature;
			donnee->temperature->temperatureMin = temperature;
			arbre = insertionTM1(donnee, arbre); // on l'insère dans l'arbre
			donnee = NULL; // on la remet à NULL pour la prochaine ligne
		}
		ligneSuitante(fichierCSV); // on passe à la ligne suivant
	}
	fclose(fichierCSV); // on ferme le fichier une fois entièrement lu

	if(arbre==NULL){ // si l'arbre est null c'est qu'on a pas pu récupéré de données
		printf("\n-!- erreur : aucune donnée correspondante trouvée -!-\n");
		return EXIT_FAILURE; // donc on peu directement quitter
	}

	fichierCSV = fopen(fichierSortie, "w"); // on ouvre en écriture le fichier de sortie

	if(fichierCSV==NULL){ // si on a pas réussi alors erreur et on termine la fonction
		printf("\n-!- erreur : le fichier de sortie n'a pas pu être ouvert ni créé -!-\n");
		return EXIT_FAILURE;
	}

	int retour1 = 1; // on définie le premier retour comme ok
	if(fprintf(fichierCSV, "# id de station;température minimale;température maximale;température moyenne\n") == EOF){retour1 = 0;} // mais si on arrive pas a écrire l'en-tête on met ce premier retour à faux (0)

	int retour2 = ecrireTM1(fichierCSV, arbre); // on remplit le fichier avec l'arbre
	if(retour1==0 || retour2==0){ // si on a pas réussi erreur
		printf("\n-!- erreur dans l'écriture du fichier data %s -!-\n", fichierSortie);
		retour=EXIT_FAILURE;
	} else if(retour1==1 || retour2==1){ // si on a réussi c ok
		retour=EXIT_SUCCESS;
	} else{retour=3;} // si aucun des 2 c'est qu'il y a une erreur pas prévue (interne)
	fclose(fichierCSV); // on ferme le fichier

	arbre = viderArbreTM1(arbre); // on vide l'arbre
	return retour; // on quitte la fonction
}

/*
	Auteur : Dorian BARRERE
	Date : 28-12-2022
	Résumer : générateur TM2
	Entrée : le filtre, les 2 chaînes pour le fichier "entrée" et le fichier "sortie"
	Sortie : 0 si ok, 1 si pas bon, 3 si pas bon mais par une erreur interne qui ne devraient pas arriver
*/
int temperatureMode2(FILTRE filtre, char* fichierEntree, char* fichierSortie){ // exactement même principe que temperatureMode1
	ARBRE arbre = NULL;
	DONNEE* donnee;
	double temperature, latitude, longitude;
	int annee, mois, jour, heure, retour;
	
	if(estDossier(fichierEntree)){
		printf("\n-!- erreur : le fichier des données n'a pas pu être ouvert, vous avez spécifié un dossier (répertoire) -!-\n");
		return EXIT_FAILURE;
	}

	FILE* fichierCSV = fopen(fichierEntree, "r");
	if(fichierCSV==NULL){
		printf("\n-!- erreur : le fichier des données n'a pas pu être ouvert -!-\n");
		return EXIT_FAILURE;
	}

	fscanf(fichierCSV, "%*[^\n]\n");
	while(!feof(fichierCSV)){
		if(recupererTM2(fichierCSV, &annee, &mois, &jour, &heure, &latitude, &longitude, &temperature) && appartientAuFiltre(filtre, latitude, longitude, annee, mois, jour)){
			donnee = malloc(sizeof(DONNEE));
			donnee->temperature = malloc(sizeof(TEMPERATURE));
			donnee->temperature->sommeTemperatures = temperature;
			donnee->temperature->nombreMesures = 1;
			donnee->date = malloc(sizeof(DATE));
			donnee->date->annee = annee;
			donnee->date->mois = mois;
			donnee->date->jour = jour;
			donnee->date->heure = heure;
			arbre = insertionTM2(donnee, arbre);
			donnee = NULL;
		}
		ligneSuitante(fichierCSV);
	}
	fclose(fichierCSV);

	if(arbre==NULL){
		printf("\n-!- erreur : aucune donnée correspondante trouvée -!-\n");
		return EXIT_FAILURE;
	}

	fichierCSV = fopen(fichierSortie, "w");

	if(fichierCSV==NULL){
		printf("\n-!- erreur : le fichier de sortie n'a pas pu être ouvert ni créé -!-\n");
		return EXIT_FAILURE;
	}

	int retour1 = 1;
	if(fprintf(fichierCSV, "# date;température moyenne\n") == EOF){retour1 = 0;}

	int retour2 = ecrireTM2(fichierCSV, arbre);
	if(retour1==0 || retour2==0){
		printf("\n-!- erreur dans l'écriture du fichier data %s -!-\n", fichierSortie);
		retour=EXIT_FAILURE;
	} else if(retour1==1 || retour2==1){
		retour=EXIT_SUCCESS;
	} else{retour=3;}
	fclose(fichierCSV);

	arbre = viderArbreTM2(arbre);
	return retour;
}

/*
	Auteur : Dorian BARRERE
	Date : 28-12-2022
	Résumer : générateur TM3
	Entrée : le filtre, les 2 chaînes pour le fichier "entrée" et le fichier "sortie"
	Sortie : 0 si ok, 1 si pas bon, 3 si pas bon mais par une erreur interne qui ne devraient pas arriver
*/
int temperatureMode3(FILTRE filtre, char* fichierEntree, char* fichierSortie){ // exactement même principe que temperatureMode1
	ARBRE arbre = NULL;
	DONNEE* donnee;
	long int idStation;
	int annee, mois, jour, heure, retour;
	double temperature, latitude, longitude;
	
	if(estDossier(fichierEntree)){
		printf("\n-!- erreur : le fichier des données n'a pas pu être ouvert, vous avez spécifié un dossier (répertoire) -!-\n");
		return EXIT_FAILURE;
	}

	FILE* fichierCSV = fopen(fichierEntree, "r");
	if(fichierCSV==NULL){
		printf("\n-!- erreur : le fichier des données n'a pas pu être ouvert -!-\n");
		return EXIT_FAILURE;
	}

	fscanf(fichierCSV, "%*[^\n]\n");
	while(!feof(fichierCSV)){
		if(recupererTM3(fichierCSV, &idStation, &annee, &mois, &jour, &heure, &latitude, &longitude, &temperature) && appartientAuFiltre(filtre, latitude, longitude, annee, mois, jour)){
			donnee = malloc(sizeof(DONNEE));
			donnee->idStation = idStation;
			donnee->temperature = malloc(sizeof(TEMPERATURE));
			donnee->temperature->sommeTemperatures = temperature;
			donnee->date = malloc(sizeof(DATE));
			donnee->date->annee = annee;
			donnee->date->mois = mois;
			donnee->date->jour = jour;
			donnee->date->heure = heure;
			arbre = insertionTM3(donnee, arbre);
			donnee = NULL;
		}
		ligneSuitante(fichierCSV);
	}
	fclose(fichierCSV);

	if(arbre==NULL){
		printf("\n-!- erreur : aucune donnée correspondante trouvée -!-\n");
		return EXIT_FAILURE;
	}

	fichierCSV = fopen(fichierSortie, "w");

	if(fichierCSV==NULL){
		printf("\n-!- erreur : le fichier de sortie n'a pas pu être ouvert ni créé -!-\n");
		return EXIT_FAILURE;
	}

	int retour1 = 1;
	if(fprintf(fichierCSV, "# date (année-mois-jour);heure (24h);température;id de station\n") == EOF){retour1 = 0;}

	int retour2 = ecrireTM3(fichierCSV, arbre);
	if(retour1==0 || retour2==0){
		printf("\n-!- erreur dans l'écriture du fichier data %s -!-\n", fichierSortie);
		retour=EXIT_FAILURE;
	} else if(retour1==1 || retour2==1){
		retour=EXIT_SUCCESS;
	} else{retour=3;}
	fclose(fichierCSV);

	arbre = viderArbreTM3(arbre);
	return retour;
}

int temperature(int mode, FILTRE filtre, char* fichierEntree, char* fichierSortie){
	int retour = 3; // on met le retour à 3 mais il va changer sinon erreur interne
	switch(mode){ // suivant le mode choisi on appelle la bonne fonction
		case 1:
			retour = temperatureMode1(filtre, fichierEntree, fichierSortie);
			break;
		case 2:
			retour = temperatureMode2(filtre, fichierEntree, fichierSortie);
			break;
		case 3:
			retour = temperatureMode3(filtre, fichierEntree, fichierSortie);
			break;
	}
	return retour;
}

/*
	Auteur : Dorian BARRERE
	Date : 29-12-2022
	Résumer : générateur PM1
	Entrée : le filtre, les 2 chaînes pour le fichier "entrée" et le fichier "sortie"
	Sortie : 0 si ok, 1 si pas bon, 3 si pas bon mais par une erreur interne qui ne devraient pas arriver
*/
int pressionMode1(FILTRE filtre, char* fichierEntree, char* fichierSortie){ // exactement même principe que temperatureMode1
	ARBRE arbre = NULL;
	DONNEE* donnee;
	long int idStation;
	double pression, latitude, longitude;
	int annee, mois, jour, retour;
	
	if(estDossier(fichierEntree)){
		printf("\n-!- erreur : le fichier des données n'a pas pu être ouvert, vous avez spécifié un dossier (répertoire) -!-\n");
		return EXIT_FAILURE;
	}

	FILE* fichierCSV = fopen(fichierEntree, "r");
	if(fichierCSV==NULL){
		printf("\n-!- erreur : le fichier des données n'a pas pu être ouvert -!-\n");
		return EXIT_FAILURE;
	}

	fscanf(fichierCSV, "%*[^\n]\n");
	while(!feof(fichierCSV)){
		if(recupererPM1(fichierCSV, &idStation, &annee, &mois, &jour, &latitude, &longitude, &pression) && appartientAuFiltre(filtre, latitude, longitude, annee, mois, jour)){
			donnee = malloc(sizeof(DONNEE));
			donnee->idStation = idStation;
			donnee->pression = malloc(sizeof(PRESSION));
			donnee->pression->sommePressions = pression;
			donnee->pression->nombreMesures = 1;
			donnee->pression->pressionMax = pression;
			donnee->pression->pressionMin = pression;
			arbre = insertionPM1(donnee, arbre);
			donnee = NULL;
		}
		ligneSuitante(fichierCSV);
	}
	fclose(fichierCSV);

	if(arbre==NULL){
		printf("\n-!- erreur : aucune donnée correspondante trouvée -!-\n");
		return EXIT_FAILURE;
	}

	fichierCSV = fopen(fichierSortie, "w");

	if(fichierCSV==NULL){
		printf("\n-!- erreur : le fichier de sortie n'a pas pu être ouvert ni créé -!-\n");
		return EXIT_FAILURE;
	}

	int retour1 = 1;
	if(fprintf(fichierCSV, "# id de station;pression minimale;pression maximale;pression moyenne\n") == EOF){retour1 = 0;}

	int retour2 = ecrirePM1(fichierCSV, arbre);
	if(retour1==0 || retour2==0){
		printf("\n-!- erreur dans l'écriture du fichier data %s -!-\n", fichierSortie);
		retour=EXIT_FAILURE;
	} else if(retour1==1 || retour2==1){
		retour=EXIT_SUCCESS;
	} else{retour=3;}
	fclose(fichierCSV);

	arbre = viderArbrePM1(arbre);
	return retour;
}

/*
	Auteur : Dorian BARRERE
	Date : 29-12-2022
	Résumer : générateur PM2
	Entrée : le filtre, les 2 chaînes pour le fichier "entrée" et le fichier "sortie"
	Sortie : 0 si ok, 1 si pas bon, 3 si pas bon mais par une erreur interne qui ne devraient pas arriver
*/
int pressionMode2(FILTRE filtre, char* fichierEntree, char* fichierSortie){ // exactement même principe que temperatureMode1
	ARBRE arbre = NULL;
	DONNEE* donnee;
	double pression, latitude, longitude, retour;
	int annee, mois, jour, heure;
	
	if(estDossier(fichierEntree)){
		printf("\n-!- erreur : le fichier des données n'a pas pu être ouvert, vous avez spécifié un dossier (répertoire) -!-\n");
		return EXIT_FAILURE;
	}

	FILE* fichierCSV = fopen(fichierEntree, "r");
	if(fichierCSV==NULL){
		printf("\n-!- erreur : le fichier des données n'a pas pu être ouvert -!-\n");
		return EXIT_FAILURE;
	}

	fscanf(fichierCSV, "%*[^\n]\n");
	while(!feof(fichierCSV)){
		if(recupererPM2(fichierCSV, &annee, &mois, &jour, &heure, &latitude, &longitude, &pression) && appartientAuFiltre(filtre, latitude, longitude, annee, mois, jour)){
			donnee = malloc(sizeof(DONNEE));
			donnee->pression = malloc(sizeof(PRESSION));
			donnee->pression->sommePressions = pression;
			donnee->pression->nombreMesures = 1;
			donnee->date = malloc(sizeof(DATE));
			donnee->date->annee = annee;
			donnee->date->mois = mois;
			donnee->date->jour = jour;
			donnee->date->heure = heure;
			arbre = insertionPM2(donnee, arbre);
			donnee = NULL;
		}
		ligneSuitante(fichierCSV);
	}
	fclose(fichierCSV);

	if(arbre==NULL){
		printf("\n-!- erreur : aucune donnée correspondante trouvée -!-\n");
		return EXIT_FAILURE;
	}

	fichierCSV = fopen(fichierSortie, "w");

	if(fichierCSV==NULL){
		printf("\n-!- erreur : le fichier de sortie n'a pas pu être ouvert ni créé -!-\n");
		return EXIT_FAILURE;
	}

	int retour1 = 1;
	if(fprintf(fichierCSV, "# date;pression moyenne\n") == EOF){retour1 = 0;}

	int retour2 = ecrirePM2(fichierCSV, arbre);
	if(retour1==0 || retour2==0){
		printf("\n-!- erreur dans l'écriture du fichier data %s -!-\n", fichierSortie);
		retour=EXIT_FAILURE;
	} else if(retour1==1 || retour2==1){
		retour=EXIT_SUCCESS;
	} else{retour=3;}
	fclose(fichierCSV);

	arbre = viderArbrePM2(arbre);
	return retour;
}

/*
	Auteur : Dorian BARRERE
	Date : 29-12-2022
	Résumer : générateur PM3
	Entrée : le filtre, les 2 chaînes pour le fichier "entrée" et le fichier "sortie"
	Sortie : 0 si ok, 1 si pas bon, 3 si pas bon mais par une erreur interne qui ne devraient pas arriver
*/
int pressionMode3(FILTRE filtre, char* fichierEntree, char* fichierSortie){ // exactement même principe que temperatureMode1
	ARBRE arbre = NULL;
	DONNEE* donnee;
	long int idStation;
	int annee, mois, jour, heure, retour;
	double pression, latitude, longitude;
	
	if(estDossier(fichierEntree)){
		printf("\n-!- erreur : le fichier des données n'a pas pu être ouvert, vous avez spécifié un dossier (répertoire) -!-\n");
		return EXIT_FAILURE;
	}

	FILE* fichierCSV = fopen(fichierEntree, "r");
	if(fichierCSV==NULL){
		printf("\n-!- erreur : le fichier des données n'a pas pu être ouvert -!-\n");
		return EXIT_FAILURE;
	}

	fscanf(fichierCSV, "%*[^\n]\n");
	while(!feof(fichierCSV)){
		if(recupererPM3(fichierCSV, &idStation, &annee, &mois, &jour, &heure, &latitude, &longitude, &pression) && appartientAuFiltre(filtre, latitude, longitude, annee, mois, jour)){
			donnee = malloc(sizeof(DONNEE));
			donnee->idStation = idStation;
			donnee->pression = malloc(sizeof(PRESSION));
			donnee->pression->sommePressions = pression;
			donnee->date = malloc(sizeof(DATE));
			donnee->date->annee = annee;
			donnee->date->mois = mois;
			donnee->date->jour = jour;
			donnee->date->heure = heure;
			arbre = insertionPM3(donnee, arbre);
			donnee = NULL;
		}
		ligneSuitante(fichierCSV);
	}
	fclose(fichierCSV);

	if(arbre==NULL){
		printf("\n-!- erreur : aucune donnée correspondante trouvée -!-\n");
		return EXIT_FAILURE;
	}

	fichierCSV = fopen(fichierSortie, "w");

	if(fichierCSV==NULL){
		printf("\n-!- erreur : le fichier de sortie n'a pas pu être ouvert ni créé -!-\n");
		return EXIT_FAILURE;
	}

	int retour1 = 1;
	if(fprintf(fichierCSV, "# date (année-mois-jour);heure (24h);pression;id de station\n") == EOF){retour1 = 0;}

	int retour2 = ecrirePM3(fichierCSV, arbre);
	if(retour1==0 || retour2==0){
		printf("\n-!- erreur dans l'écriture du fichier data %s -!-\n", fichierSortie);
		retour=EXIT_FAILURE;
	} else if(retour1==1 || retour2==1){
		retour=EXIT_SUCCESS;
	} else{retour=3;}
	fclose(fichierCSV);

	arbre = viderArbrePM3(arbre);
	return retour;
}

int pression(int mode, FILTRE filtre, char* fichierEntree, char* fichierSortie){
	int retour = 3; // on met le retour à 3 mais il va changer sinon erreur interne
	switch(mode){ // suivant le mode choisi on appelle la bonne fonction
		case 1:
			retour = pressionMode1(filtre, fichierEntree, fichierSortie);
			break;
		case 2:
			retour = pressionMode2(filtre, fichierEntree, fichierSortie);
			break;
		case 3:
			retour = pressionMode3(filtre, fichierEntree, fichierSortie);
			break;
	}
	return retour;
}

/*
	Auteur : Dorian BARRERE
	Date : 30-12-2022
	Résumer : générateur Vent
	Entrée : le filtre, les 2 chaînes pour le fichier "entrée" et le fichier "sortie"
	Sortie : 0 si ok, 1 si pas bon, 3 si pas bon mais par une erreur interne qui ne devraient pas arriver
*/
int vent(FILTRE filtre, char* fichierEntree, char* fichierSortie){ // exactement même principe que temperatureMode1
	ARBRE arbre = NULL;
	DONNEE* donnee;
	long int idStation;
	double directionVent, vitesseVent, latitude, longitude;
	int annee, mois, jour, retour;
	
	if(estDossier(fichierEntree)){
		printf("\n-!- erreur : le fichier des données n'a pas pu être ouvert, vous avez spécifié un dossier (répertoire) -!-\n");
		return EXIT_FAILURE;
	}

	FILE* fichierCSV = fopen(fichierEntree, "r");
	if(fichierCSV==NULL){
		printf("\n-!- erreur : le fichier des données n'a pas pu être ouvert -!-\n");
		return EXIT_FAILURE;
	}

	fscanf(fichierCSV, "%*[^\n]\n");
	while(!feof(fichierCSV)){
		if(recupererVent(fichierCSV, &idStation, &annee, &mois, &jour, &directionVent, &vitesseVent, &latitude, &longitude) && appartientAuFiltre(filtre, latitude, longitude, annee, mois, jour)){
			donnee = malloc(sizeof(DONNEE));
			donnee->idStation = idStation;
			donnee->coordonnees = malloc(sizeof(COO));
			donnee->coordonnees->latitude = latitude;
			donnee->coordonnees->longitude = longitude;
			donnee->vent = malloc(sizeof(VENT));
			donnee->vent->sommeComposantesX = vitesseVent*cos(degre360EnRadianSymetrique(directionVent)); // on déduit par le calcul les composante en x
			donnee->vent->sommeComposantesY = vitesseVent*sin(degre360EnRadianSymetrique(directionVent)); // on déduit par le calcul les composante en y
			donnee->vent->nombreMesures = 1;
			arbre = insertionVent(donnee, arbre);
			donnee = NULL;
		}
		ligneSuitante(fichierCSV);
	}
	fclose(fichierCSV);

	if(arbre==NULL){
		printf("\n-!- erreur : aucune donnée correspondante trouvée -!-\n");
		return EXIT_FAILURE;
	}

	fichierCSV = fopen(fichierSortie, "w");

	if(fichierCSV==NULL){
		printf("\n-!- erreur : le fichier de sortie n'a pas pu être ouvert ni créé -!-\n");
		return EXIT_FAILURE;
	}

	int retour1 = 1;
	if(fprintf(fichierCSV, "# id de station;latitude;longitude;angle moyen en degrés [0-360];norme moyenne du vecteur vent\n") == EOF){retour1 = 0;}

	int retour2 = ecrireVent(fichierCSV, arbre);
	if(retour1==0 || retour2==0){
		printf("\n-!- erreur dans l'écriture du fichier data %s -!-\n", fichierSortie);
		retour=EXIT_FAILURE;
	} else if(retour1==1 || retour2==1){
		retour=EXIT_SUCCESS;
	} else{retour=3;}
	fclose(fichierCSV);

	arbre = viderArbreVent(arbre);
	return retour;
}

/*
	Auteur : Dorian BARRERE
	Date : 31-12-2022
	Résumer : générateur hauteur
	Entrée : le filtre, les 2 chaînes pour le fichier "entrée" et le fichier "sortie"
	Sortie : 0 si ok, 1 si pas bon, 3 si pas bon mais par une erreur interne qui ne devraient pas arriver
*/
int hauteur(FILTRE filtre, char* fichierEntree, char* fichierSortie){ // exactement même principe que temperatureMode1
	ARBRE arbre = NULL;
	DONNEE* donnee;
	double latitude, longitude, altitude;
	int annee, mois, jour, retour;
	
	if(estDossier(fichierEntree)){
		printf("\n-!- erreur : le fichier des données n'a pas pu être ouvert, vous avez spécifié un dossier (répertoire) -!-\n");
		return EXIT_FAILURE;
	}

	FILE* fichierCSV = fopen(fichierEntree, "r");
	if(fichierCSV==NULL){
		printf("\n-!- erreur : le fichier des données n'a pas pu être ouvert -!-\n");
		return EXIT_FAILURE;
	}

	fscanf(fichierCSV, "%*[^\n]\n");
	while(!feof(fichierCSV)){
		if(recupererHauteur(fichierCSV, &annee, &mois, &jour, &latitude, &longitude, &altitude) && appartientAuFiltre(filtre, latitude, longitude, annee, mois, jour) && !stationDejaPresente(arbre, latitude, longitude, altitude)){  // l'équilibrage avl ne conserve précisément QUE l'ordre strict donc on est obligé de vérifier si la station est déjà présente avant "d'entrer" dans l'arbre
			donnee = malloc(sizeof(DONNEE));
			donnee->coordonnees = malloc(sizeof(COO));
			donnee->coordonnees->latitude = latitude;
			donnee->coordonnees->longitude = longitude;
			donnee->coordonnees->altitude = altitude;
			arbre = insertionHauteur(donnee, arbre);
			donnee = NULL;
		}
		ligneSuitante(fichierCSV);
	}
	fclose(fichierCSV);

	if(arbre==NULL){
		printf("\n-!- erreur : aucune donnée correspondante trouvée -!-\n");
		return EXIT_FAILURE;
	}

	fichierCSV = fopen(fichierSortie, "w");

	if(fichierCSV==NULL){
		printf("\n-!- erreur : le fichier de sortie n'a pas pu être ouvert ni créé -!-\n");
		return EXIT_FAILURE;
	}

	int retour1 = 1;
	if(fprintf(fichierCSV, "# latitude;longitude;altitude\n") == EOF){retour1 = 0;}

	int retour2 = ecrireHauteur(fichierCSV, arbre);
	if(retour1==0 || retour2==0){
		printf("\n-!- erreur dans l'écriture du fichier data %s -!-\n", fichierSortie);
		retour=EXIT_FAILURE;
	} else if(retour1==1 || retour2==1){
		retour=EXIT_SUCCESS;
	} else{retour=3;}
	fclose(fichierCSV);

	arbre = viderArbreHauteur(arbre);
	return retour;
}

/*
	Auteur : Dorian BARRERE
	Date : 31-12-2022
	Résumer : générateur Humidité
	Entrée : le filtre, les 2 chaînes pour le fichier "entrée" et le fichier "sortie"
	Sortie : 0 si ok, 1 si pas bon, 3 si pas bon mais par une erreur interne qui ne devraient pas arriver
*/
int humidite(FILTRE filtre, char* fichierEntree, char* fichierSortie){ // même principe que temperatureMode1 avec quand même une petite subtilité en plus
	ARBRE arbre = NULL;
	DONNEE* donnee;
	long int idStation; // on va faire 2 arbres pour dans un premier temps avoir l'humidité max par station, puis dans un second temps les trier par humidité max décrossantes
	double latitude, longitude;
	float humidite;
	int annee, mois, jour, retour;

	if(estDossier(fichierEntree)){
		printf("\n-!- erreur : le fichier des données n'a pas pu être ouvert, vous avez spécifié un dossier (répertoire) -!-\n");
		return EXIT_FAILURE;
	}

	FILE* fichierCSV = fopen(fichierEntree, "r");
	if(fichierCSV==NULL){
		printf("\n-!- erreur : le fichier des données n'a pas pu être ouvert -!-\n");
		return EXIT_FAILURE;
	}

	fscanf(fichierCSV, "%*[^\n]\n");
	while(!feof(fichierCSV)){
		if(recupererHumidite(fichierCSV, &idStation, &annee, &mois, &jour, &humidite, &latitude, &longitude) && appartientAuFiltre(filtre, latitude, longitude, annee, mois, jour)){
			donnee = malloc(sizeof(DONNEE));
			donnee->idStation = idStation;
			donnee->humiditeMax = humidite;
			donnee->coordonnees = malloc(sizeof(COO));
			donnee->coordonnees->latitude = latitude;
			donnee->coordonnees->longitude = longitude;
			arbre = insertionHumidite(donnee, arbre);
			donnee = NULL;
		}
		ligneSuitante(fichierCSV);
	}
	fclose(fichierCSV);

	if(arbre==NULL){
		printf("\n-!- erreur : aucune donnée correspondante trouvée -!-\n");
		return EXIT_FAILURE;
	}

	trierHumidite(&arbre); // on a actuellement l'arbre trié par ids de station décroissantes ce que nous permettait de vérifier si une station était déjà présente directement pendant l'insertion (contrairement au générateur hauteur !), donc on doit le trier ici suivant le humidités décroissantes

	fichierCSV = fopen(fichierSortie, "w");

	if(fichierCSV==NULL){
		printf("\n-!- erreur : le fichier de sortie n'a pas pu être ouvert ni créé -!-\n");
		return EXIT_FAILURE;
	}

	int retour1 = 1;
	if(fprintf(fichierCSV, "# latitude;longitude;humidité maximale\n") == EOF){retour1 = 0;}

	int retour2 = ecrireHumidite(fichierCSV, arbre);
	if(retour1==0 || retour2==0){
		printf("\n-!- erreur dans l'écriture du fichier data %s -!-\n", fichierSortie);
		retour=EXIT_FAILURE;
	} else if(retour1==1 || retour2==1){
		retour=EXIT_SUCCESS;
	} else{retour=3;}
	fclose(fichierCSV);

	arbre = viderArbreHumidite(arbre);
	return retour;
}
