/* on inclu tout ce dont à a besoin */
#include "librairiesBasiques.h"
#include "commandes.h"
#include "utilitaires.h"

void aideApplication(){ // suite d'affichages dans la console
	printf("\nVeuillez lire attentivement l'aide des options ci-dessous :\n\n");
	printf("	--help : affiche l'aide détaillée\n");
	printf("	-t <mode> : créer le diagramme des températures\n");
	printf("	-p <mode> : créer le diagramme de pressions atmosphériques\n");
	printf("		ATTENTION Pour les 2 options précédentes, il faut OBLIGATOIREMENT indiquer un <mode> :\n");
	printf("			1 = type “barres d’erreur”\n");
	printf("			2 = type “ligne simple”\n");
	printf("			3 = type “multi-lignes”\n");
	printf("	-w : créer le diagramme du vent de type “vecteurs”\n");
	printf("	-h : créer le diagramme hauteur de type “carte interpolée et colorée”\n" );
	printf("	-m : créer le diagramme de l'humidité de type “carte interpolée et colorée”\n");
	printf("		ATTENTION Au moins l'une des ces 5 dernières options doit être présente.\n");
	printf("	-g <min> <max> : permet de ne conserver que les données qui sont dans l’intervalle de longitudes [<min>..<max>]\n");
	printf("	-a <min> <max> : permet de ne conserver que les données qui sont dans l’intervalle de latitudes [<min>..<max>]\n");
	printf("	-d <min> <max> : permet de ne conserver que les données qui sont dans l’intervalle de dates [<min>..<max>]. Le format des dates est YYYY-MM-DD (année-mois-jour)\n");
	printf("		ATTENTION Ces 3 dernières options -g, -a et -d sont optionnelles.\n");
	printf("		ATTENTION Les dates des mesures doivent être plus récentes que 1 Janvier 1970.\n");
	printf("	-i <nom_fichier>.csv : permet de spécifier le chemin du fichier CSV d’entrée (son adresse depuis le dossier courant). Cette option est OBLIGATOIRE\n");
	printf("	-o <nom_fichier> : permet de donner un nom au fichier de sortie contenant les données (lors de l'appel de application.sh il se génère par défaut dans le répertoire fichiersGeneres, mais lors de l'appel direct de l'exécutable il est généré par défaut directement dans le dossier courant). ATTENTION L'extension .dat sera automatiquement rajoutée\n");
	printf("		ATTENTION Les options suivantes -F, -G, -S, -A, -O et -Q sont exclusives et ne sont pas compatibles avec -g et -a.\n");
	printf("	-F : limite les mesures à celles présentes en France métropolitaine et la Corse\n");
	printf("	-G : limite les mesures à celles présentes en Guyane\n");
	printf("	-S : limite les mesures à celles présentes sur l'île Saint-Pierre et Miquelon\n");
	printf("	-A : limite les mesures à celles présentes aux Antilles\n");
	printf("	-O : limite les mesures à celles présentes dans l’Océan Indien\n");
	printf("	-Q : limite les mesures à celles présentes en Antarctique\n");
}

/*
	Auteur : Dorian BARRERE
	Date : 04-12-2022
	Résumer : automatise la fin de la fonction en libérant
	Entrée : la valeur de retour souhaitée, le pointeur de pointeur vers la liste d'instructions, la chaine à libérer
	Sortie : la valeur de retour entrée en paramètre
*/
int finDeRecupererInstructions(int valeurRetour, LISTEINSTRUCTIONS** var1, char* var2){
	if(valeurRetour == 0){ // si on veut retourner une erreur on libère tout avant
		free((*var1)->iValeur);
		free((*var1)->oValeur);
		free(*var1);
		*var1 = NULL;
	}
	free(var2); // sinon on garde la liste d'instructions intacte
	return valeurRetour;
}

/*
	Auteur : Dorian BARRERE
	Date : 04-12-2022
	Résumer : réinitialise la liste d'instruction aux valeurs par défaut (0)
	Entrée : le pointeur vers la liste d'instructions
	Sortie : rien
*/
void reInitialiserListeInstructions(LISTEINSTRUCTIONS* listeInstructions){
	/* on affecte tout à 0 */
	listeInstructions->aideDrapeau = 0;
	listeInstructions->tDrapeau = 0;
	listeInstructions->tValeur = 0;
	listeInstructions->pDrapeau = 0;
	listeInstructions->pValeur = 0;
	listeInstructions->wDrapeau = 0;
	listeInstructions->hDrapeau = 0;
	listeInstructions->mDrapeau = 0;
	listeInstructions->gDrapeau = 0;
	listeInstructions->gValeurMin = 0;
	listeInstructions->gValeurMax = 0;
	listeInstructions->aDrapeau = 0;
	listeInstructions->aValeurMin = 0;
	listeInstructions->aValeurMax = 0;
	listeInstructions->dDrapeau = 0;
	listeInstructions->dValeurMin.annee = 0;
	listeInstructions->dValeurMin.mois = 0;
	listeInstructions->dValeurMin.jour = 0;
	listeInstructions->dValeurMin.heure = 0;
	listeInstructions->dValeurMax.annee = 0;
	listeInstructions->dValeurMax.mois = 0;
	listeInstructions->dValeurMax.jour = 0;
	listeInstructions->dValeurMax.heure = 0;
	listeInstructions->iDrapeau = 0;
	listeInstructions->oDrapeau = 0;
}

int recupererInstructions(const int argc, const char* const argv[], LISTEINSTRUCTIONS** listeInstructions){
	/* avant tout, si il n'y a aucun argument/option, cela ne sert à rien de continuer, on affiche directement l'aide */
	if(argc == 1){
		return 0;
	}

	/* on déclare et affecte les variables nécessaires pour la suite */
	(*listeInstructions) = malloc(sizeof(LISTEINSTRUCTIONS));
	reInitialiserListeInstructions(*listeInstructions);
	(*listeInstructions)->iValeur = malloc(128*sizeof(char));
	(*listeInstructions)->oValeur = malloc(128*sizeof(char));
	char* chaineLecture = malloc(128*sizeof(char)); // pour lire la commande
	char* chaineFinDeLecture; // pour regarder où on s'est arrêté dans la lecture
	char option; // pour savoir quelle option on a en face

	for(int indiceArgument=1 ; indiceArgument<argc ; indiceArgument++){ // on parcours toutes les cases de argv

		if(sscanf(argv[indiceArgument], "-%127[^;]", chaineLecture) && strlen(chaineLecture)==1){ // si la case correspond à une option
			option = chaineLecture[0]; // on la récupère
		} else{ // sinon
			if(!strcmp(argv[indiceArgument], "--help")){ // si c'est --help c ok
				reInitialiserListeInstructions(*listeInstructions); // on annule tout
				(*listeInstructions)->aideDrapeau = 1; // on dit qu'on veut l'aide
				return finDeRecupererInstructions(1, listeInstructions, chaineLecture); // et on termine la fonction
			}
			return finDeRecupererInstructions(0, listeInstructions, chaineLecture); // mais si c'est pas --help c'est la commande n'est pas valide
		}

		switch(option){ // suivant l'option récupérée on fait notre analyse
			case 't': // -t alors 1 argument à la suite
				indiceArgument++; // on va le chercher
				if((*listeInstructions)->tDrapeau == 1 || indiceArgument >= argc || strlen(argv[indiceArgument])!=1 || argv[indiceArgument][0]<49 || argv[indiceArgument][0]>51){return finDeRecupererInstructions(0, listeInstructions, chaineLecture);} // on vérifie que ya pas de soucis
				(*listeInstructions)->tValeur = atoi(argv[indiceArgument]); // on le récupère
				(*listeInstructions)->tDrapeau = 1; // on dit qu'on l'a récupéré
				break;
			case 'p': // -t alors 1 argument à la suite, pareil que -t
				indiceArgument++;
				if((*listeInstructions)->pDrapeau == 1 || indiceArgument >= argc || strlen(argv[indiceArgument])!=1 || argv[indiceArgument][0]<49 || argv[indiceArgument][0]>51){return finDeRecupererInstructions(0, listeInstructions, chaineLecture);}
				(*listeInstructions)->pValeur = atoi(argv[indiceArgument]);
				(*listeInstructions)->pDrapeau = 1;
				break;
			case 'w': // -w ya rien à la suite
				if((*listeInstructions)->wDrapeau == 1){return finDeRecupererInstructions(0, listeInstructions, chaineLecture);}
				(*listeInstructions)->wDrapeau = 1;
				break;
			case 'h': // -h ya rien à la suite
				if((*listeInstructions)->hDrapeau == 1){return finDeRecupererInstructions(0, listeInstructions, chaineLecture);}
				(*listeInstructions)->hDrapeau = 1;
				break;
			case 'm': // -m ya rien à la suite
				if((*listeInstructions)->mDrapeau == 1){return finDeRecupererInstructions(0, listeInstructions, chaineLecture);}
				(*listeInstructions)->mDrapeau = 1;
				break;
			case 'g': // -g il y a 2 arguments à la suite
				/* on récupère le premier */
				indiceArgument++;
				if((*listeInstructions)->gDrapeau == 1 || indiceArgument >= argc){return finDeRecupererInstructions(0, listeInstructions, chaineLecture);}
				(*listeInstructions)->gValeurMin = strtod(argv[indiceArgument], &chaineFinDeLecture);
				if(chaineFinDeLecture[0]!=0){return finDeRecupererInstructions(0, listeInstructions, chaineLecture);}

				/* on récupère le second */
				indiceArgument++;
				if(indiceArgument >= argc){return finDeRecupererInstructions(0, listeInstructions, chaineLecture);}
				(*listeInstructions)->gValeurMax = strtod(argv[indiceArgument], &chaineFinDeLecture);
				if(chaineFinDeLecture[0]!=0 || (*listeInstructions)->gValeurMin > (*listeInstructions)->gValeurMax || (*listeInstructions)->gValeurMin<-180 || (*listeInstructions)->gValeurMax>180){return finDeRecupererInstructions(0, listeInstructions, chaineLecture);}
				
				(*listeInstructions)->gDrapeau = 1; // on dit qu'on les a récupérés
				break;
			case 'a': // -a il y a 2 arguments à la suite
				/* on récupère le premier */
				indiceArgument++;
				if((*listeInstructions)->aDrapeau == 1 || indiceArgument >= argc){return finDeRecupererInstructions(0, listeInstructions, chaineLecture);}
				(*listeInstructions)->aValeurMin = strtod(argv[indiceArgument], &chaineFinDeLecture);
				if(chaineFinDeLecture[0]!=0){return finDeRecupererInstructions(0, listeInstructions, chaineLecture);}

				/* on récupère le second */
				indiceArgument++;
				if(indiceArgument >= argc){return finDeRecupererInstructions(0, listeInstructions, chaineLecture);}
				(*listeInstructions)->aValeurMax = strtod(argv[indiceArgument], &chaineFinDeLecture);
				if(chaineFinDeLecture[0]!=0 || (*listeInstructions)->aValeurMin > (*listeInstructions)->aValeurMax || (*listeInstructions)->aValeurMin<-90 || (*listeInstructions)->aValeurMax>90){return finDeRecupererInstructions(0, listeInstructions, chaineLecture);}
				
				(*listeInstructions)->aDrapeau = 1; // on dit qu'on les a récupérés
				break;
			case 'd': // -d il y a 2 arguments à la suite
				/* on récupère le premier */
				indiceArgument++;
				if((*listeInstructions)->dDrapeau == 1 || indiceArgument >= argc || sscanf(argv[indiceArgument], "%d-%d-%d%1[^\n]", &(*listeInstructions)->dValeurMin.annee, &(*listeInstructions)->dValeurMin.mois, &(*listeInstructions)->dValeurMin.jour, chaineLecture)!=3){return finDeRecupererInstructions(0, listeInstructions, chaineLecture);}
				
				/* on récupère le second */
				indiceArgument++;
				if(indiceArgument >= argc || sscanf(argv[indiceArgument], "%d-%d-%d%1[^\n]", &(*listeInstructions)->dValeurMax.annee, &(*listeInstructions)->dValeurMax.mois, &(*listeInstructions)->dValeurMax.jour, chaineLecture)!=3 || estApres((*listeInstructions)->dValeurMin, (*listeInstructions)->dValeurMax) || !estValide((*listeInstructions)->dValeurMin) || !estValide((*listeInstructions)->dValeurMax)){return finDeRecupererInstructions(0, listeInstructions, chaineLecture);}
				
				(*listeInstructions)->dDrapeau = 1; // on dit qu'on les a récupérés
				break;
			case 'i': // -i donc 1 argument à la suite
				indiceArgument++;
				if((*listeInstructions)->iDrapeau == 1 || indiceArgument >= argc || sscanf(argv[indiceArgument], "%127[^\n]%1[^\n]", (*listeInstructions)->iValeur, chaineLecture)!=1){return finDeRecupererInstructions(0, listeInstructions, chaineLecture);}
				(*listeInstructions)->iDrapeau = 1;
				break;
			case 'o': // -o donc 1 argument à la suite
				indiceArgument++;
				if((*listeInstructions)->oDrapeau == 1 || indiceArgument >= argc || sscanf(argv[indiceArgument], "%127[^\n]%1[^\n]", (*listeInstructions)->oValeur, chaineLecture)!=1){return finDeRecupererInstructions(0, listeInstructions, chaineLecture);}
				(*listeInstructions)->oDrapeau = 1;
				break;
			default: // l'option n'est pas reconnue donc erreur
				return finDeRecupererInstructions(0, listeInstructions, chaineLecture);
		}
	}
	return (finDeRecupererInstructions(1, listeInstructions, chaineLecture) && ((*listeInstructions)->tDrapeau + (*listeInstructions)->pDrapeau + (*listeInstructions)->wDrapeau + (*listeInstructions)->hDrapeau + (*listeInstructions)->mDrapeau) == 1 && (*listeInstructions)->iDrapeau); // on renvoie la liste d'instructions tout en vérifiant que tout est bon
}