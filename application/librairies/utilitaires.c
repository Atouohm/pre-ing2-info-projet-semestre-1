/* on inclu tout ce dont à a besoin */
#include "librairiesBasiques.h"
#include "utilitaires.h"
#define PI (double)(3.1415926535897932384626433832795) // on fait un define de nombre pi assez précis pour la clarté du code

/*
	Auteur : Julien Gitton
	Date : 04/01/2023
	Résumé : permet de normaliser les heures sous différents fuseaux horaires en UTC +00h00
	Entrée(s) : pointeurs d'entier: annee, mois, jour, heure déjà initialisés et entier: minute, seconde, fuseauHeure, fuseauMinute
	Sorties(s) : la valeur de l'annee, du mois, du jour et de l'heure est mise à jour
*/
void harmoniserDate(int* annee, int* mois, int* jour, int* heure, int minute, int seconde, int fuseauHeure, int fuseauMinute){
	minute -= fuseauMinute; // décalage des minutes
	if (minute < 0) { // on revient à l'heure précédente
		minute += 60;
		*heure -= 1;
	} else if (minute > 59) { // on passe à l'heure suivante
		minute -= 60;
		*heure += 1;
	}

	*heure -= fuseauHeure; // décalage de l'heure
	if (*heure < 0) { // on revient au jour précédent
		*heure += 24;
		*jour -= 1;
	} else if (*heure > 23) { // on passe au jour suivant
		*heure -= 24;
		*jour += 1;
	}

	if (*mois == 1 || *mois == 3 || *mois == 5 || *mois == 7 || *mois == 8 || *mois == 10 || *mois == 12) { //mois à 31 jours
		if (*jour > 31) { // on passe au premier jour du mois suivant
			if (*mois == 12) { // cas du mois de décembre
				*mois = 1;
				*jour = 1;
				*annee += 1;
			} else {
				*jour = 1;
				*mois += 1;
			}

		} else if (*jour < 1) { // on revient au dernier jour du mois précédent
			if (*mois == 8) { // cas du mois d'aout
				*mois -= 1;
				*jour = 31;
			} else if (*mois == 3) { // cas du mois de mars
				*mois -= 1;
				if ((((*annee % 4) == 0) && ((*annee % 100) != 0)) || ((*annee % 400) == 0)) { //cas d'une annee bissextile
					*jour = 29;
				} else { // annee non bissextile
					*jour = 28;
				}
			} else if (*mois == 1) { // cas du mois de janvier
				*annee -= 1;
				*mois -= 1;
				*jour = 31;
			} else { // mai, juillet, octobre, décembre
				*mois -= 1;
				*jour = 30;
			}
		}

	} else if (*mois == 2) { // cas du mois de fevrier
		if (*jour < 1) { // on revient au 31 janvier
			*jour = 31;
			*mois -= 1;
		} else if ((((*annee % 4) == 0) && ((*annee % 100) != 0)) || ((*annee % 400) == 0)) { //cas d'une annee bissextile
			if (*jour > 29) { // on passe au 1er mars
				*jour = 1;
				*mois += 1;
			}
		} else if (*jour > 28) { // année non bissextile, on passe au 1er mars
			*jour = 1;
			*mois += 1;
		}
	} else { // mois à 30 jours
		if (*jour < 1) { // on revient au 31 du mois précédent
			*jour = 31;
			*mois -= 1;
		} else if (*jour > 30) { // on passe au 1er du mois suivant
			*jour = 1;
			*mois += 1;
		}
	}
}

/*
	Auteur : Dorian BARRERE
	Date : 28-12-2022
	Résumer : récupère la date de la ligne actuellement lue dans le fichier csv
	Entrée : le pointeur vers le fichier csv (doit être au début d'une ligne), les pointeurs vers les variables année/mois/jour/heure
	Sortie : 1 si ok, 0 sinon
*/
int recupererDate(FILE* fichierCSV, int* annee, int* mois, int* jour, int* heure){
	/* on déclare et initialise toutes les variables à 0 sauf le compteur à 1 car on pointe déjà vers la première colonne */
	int minute = 0;
	int seconde = 0;
	int fuseauHeure = 0;
	int fuseauMinute = 0;
	int compteur = 1;

	while(compteur != 2 && !feof(fichierCSV)){ // on se rend à la colonne 2
		if(fgetc(fichierCSV) == ';'){
			compteur++;
		}
	}

	if(fscanf(fichierCSV, "%d-%d-%dT%d:%d:%d", annee, mois, jour, heure, &minute, &seconde) != 6){ // on récupère la date
		return 0; // si on a pas réussi on retourne une erreur
	} else{ // sinon on continue à l'analiser en regardant le fuseau horaire
		char caractereSuivant = fgetc(fichierCSV); // on prend le prochain caractère
		if(caractereSuivant == '+' || caractereSuivant == '-'){ // si c'est un + ou un - c ok
			if(fscanf(fichierCSV, "%d:%d", &fuseauHeure, &fuseauMinute) != 2){ // on récupère le fuseau
				return 0; // si on a pas réussi on retourne une erreur
			} else{
				switch(caractereSuivant){ // sinon on regarde si le fuseau est positif ou négatif et on ramène la date à UTC +0
					case '+':
						harmoniserDate(annee, mois, jour, heure, minute, seconde, fuseauHeure, fuseauMinute);
						break;
					case '-':
						harmoniserDate(annee, mois, jour, heure, minute, seconde, -fuseauHeure, -fuseauMinute);
						break;
				}
			}
		} else if(caractereSuivant == 'Z'){ // sinon si c'est un Z ça reste ok dans la norme ISO 8601
			harmoniserDate(annee, mois, jour, heure, minute, seconde, fuseauHeure, fuseauMinute); // on ramène la date à UTC +0
		} else{ // mais sinon c'est pas bon
			return 0; // donc on retourne une erreur
		}

		DATE date;
		date.annee = *annee;
		date.mois = *mois;
		date.jour = *jour;
		date.heure = *heure;
		return estValide(date); // on a bien réussi à récupérer la date donc on retourne ok
	}
}

int recupererTM1(FILE* fichierCSV, long int* idStation, int* annee, int* mois, int* jour, double* latitude, double* longitude, double* temperature){
    int heure; // on va utiliser recupererTM3 mais ducoup on a besoin d'une variable heure
    if(recupererTM3(fichierCSV, idStation, annee, mois, jour, &heure, latitude, longitude, temperature)){ // on récupère les même données que recupererTM3 sauf l'heure
    	return 1;
    } else{
    	return 0;
    }
}

int recupererTM2(FILE* fichierCSV, int* annee, int* mois, int* jour, int* heure, double* latitude, double* longitude, double* temperature){
    /* variables de retour pour voir si tout va bien à la fin */
    int retour1 = 0;
    int retour2 = 0;
    if(recupererDate(fichierCSV, annee, mois, jour, heure)){ // on récupère la date
    	/* on passe à la colonne suivante */
		if(fgetc(fichierCSV) != ';'){return 0;}
		int compteur = 3;

		while(compteur <= 11 && !feof(fichierCSV)){ // on parcourt toutes les colonnes jusqu'à la dernière qui nous intéresse
        	switch(compteur){ // suivant le numéro de la colonne on la récupère ou non
            	case 10: // c'est le coo
	                retour1 = fscanf(fichierCSV, "%lf,%lf", latitude, longitude);
                	/* on passe à la colonne suivante */
                	if(!retour1 || fgetc(fichierCSV) != ';'){return 0;}
                	compteur++;
               		break;
	            case 11: // c'est la température
	                retour2 = fscanf(fichierCSV, "%lf", temperature);
	                /* on passe à la colonne suivante */
	                compteur++;
	                break;
	            default:
	            	/* on passe à la colonne suivante */
	                if(fgetc(fichierCSV) == ';'){
	                    compteur++;
	                }
       		}
    	}
	} else{ // sinon c'est pas bon
		return 0;
	}
    return (retour1 && retour2); // on retourne en vérifiant si ça s'est bien passé
}

int recupererTM3(FILE* fichierCSV, long int* idStation, int* annee, int* mois, int* jour, int* heure, double* latitude, double* longitude, double* temperature){
	/* on récupère l'id de station mais on revient ensuite à la position de là où on est parti pour ne pas fausser la suite */
	fpos_t pos;
	fgetpos(fichierCSV, &pos);
    if(!fscanf(fichierCSV, "%ld", idStation) || fgetc(fichierCSV) != ';'){return 0;}
    fsetpos(fichierCSV, &pos);

    if(recupererTM2(fichierCSV, annee, mois, jour, heure, latitude, longitude, temperature)){ // on récupère les même données que recupererTM2
		return 1;
	} else{
		return 0;
	}
}

/* équivalent à recupererTM1 mais avec la pression */
int recupererPM1(FILE* fichierCSV, long int* idStation, int* annee, int* mois, int* jour, double* latitude, double* longitude, double* pression){
    int heure;
    if(recupererPM3(fichierCSV, idStation, annee, mois, jour, &heure, latitude, longitude, pression)){
    	return 1;
    } else{
    	return 0;
    }
}

/* équivalent à recupererTM2 mais avec la pression */
int recupererPM2(FILE* fichierCSV, int* annee, int* mois, int* jour, int* heure, double* latitude, double* longitude, double* pression){
	int retour1 = 0;
    int retour2 = 0;
    if(recupererDate(fichierCSV, annee, mois, jour, heure)){
		if(fgetc(fichierCSV) != ';'){return 0;}
		int compteur = 3;

		while(compteur <= 10 && !feof(fichierCSV)){
        	switch(compteur){
        		case 7:
	                retour1 = fscanf(fichierCSV, "%lf", pression);
                	if(!retour1 || fgetc(fichierCSV) != ';'){return 0;}
                	compteur++;
               		break;
            	case 10:
	                retour2 = fscanf(fichierCSV, "%lf,%lf", latitude, longitude);
                	compteur++;
               		break;
	            default:
	                if(fgetc(fichierCSV) == ';'){
	                    compteur++;
	                }
       		}
    	}
	} else{
		return 0;
	}
    return (retour1 && retour2);
}

/* équivalent à recupererTM3 mais avec la pression */
int recupererPM3(FILE* fichierCSV, long int* idStation, int* annee, int* mois, int* jour, int* heure, double* latitude, double* longitude, double* pression){
	fpos_t pos;
	fgetpos(fichierCSV, &pos);
    if(!fscanf(fichierCSV, "%ld", idStation) || fgetc(fichierCSV) != ';'){return 0;}
    fsetpos(fichierCSV, &pos);

    if(recupererPM2(fichierCSV, annee, mois, jour, heure, latitude, longitude, pression)){
		return 1;
	} else{
		return 0;
	}
}

int recupererVent(FILE* fichierCSV, long int* idStation, int* annee, int* mois, int* jour, double* directionVent, double* vitesseVent, double* latitude, double* longitude){
	/* on récupère l'id de station mais on revient ensuite à la position de là où on est parti pour ne pas fausser la suite */
	fpos_t pos;
	fgetpos(fichierCSV, &pos);
    if(!fscanf(fichierCSV, "%ld", idStation) || fgetc(fichierCSV) != ';'){return 0;}
    fsetpos(fichierCSV, &pos);

    /* même principe que recupererTM2 */
	int retour1 = 0;
    int retour2 = 0;
    int heure;
    if(recupererDate(fichierCSV, annee, mois, jour, &heure)){
		if(fgetc(fichierCSV) != ';'){return 0;}
		int compteur = 3;

		while(compteur <= 10 && !feof(fichierCSV)){
	        switch(compteur){
	            case 4:
	                retour1 = fscanf(fichierCSV, "%lf;%lf", directionVent, vitesseVent);
	                if(!retour1 || fgetc(fichierCSV) != ';' || *directionVent < 0 || *directionVent > 360){return 0;}
	                compteur = compteur+2;
	                break;
	            case 10:
	                retour2 = fscanf(fichierCSV, "%lf,%lf", latitude, longitude);
	                compteur++;
	                break;
	            default:
	                if(fgetc(fichierCSV) == ';'){
	                    compteur++;
	                }
	        }
    	}
	} else{
		return 0;
	}
    return (retour1 && (retour2 == 2));
}

/* même principe que recupererTM2 */
int recupererHauteur(FILE* fichierCSV, int* annee, int* mois, int* jour, double* latitude, double* longitude, double* altitude){
	int retour1 = 0;
    int retour2 = 0;
    int heure;
    if(recupererDate(fichierCSV, annee, mois, jour, &heure)){
		if(fgetc(fichierCSV) != ';'){return 0;}
		int compteur = 3;

		while(compteur <= 14 && !feof(fichierCSV)){
	        switch(compteur){
	            case 10:
	                retour1 = fscanf(fichierCSV, "%lf,%lf", latitude, longitude);
	                if(!retour1 || fgetc(fichierCSV) != ';'){return 0;}
	                compteur++;
	                break;
	            case 14:
	                retour2 = fscanf(fichierCSV, "%lf", altitude);
	                compteur++;
	                break;
	            default:
	                if(fgetc(fichierCSV) == ';'){
	                    compteur++;
	                }
	        }
    	}
	} else{
		return 0;
	}
    return ((retour1 == 2) && retour2);
}

/* même principe que recupererVent */
int recupererHumidite(FILE* fichierCSV, long int* idStation, int* annee, int* mois, int* jour, float* humidite, double* latitude, double* longitude){
	fpos_t pos;
	fgetpos(fichierCSV, &pos);
    if(!fscanf(fichierCSV, "%ld", idStation) || fgetc(fichierCSV) != ';'){return 0;}
    fsetpos(fichierCSV, &pos);

	int retour1 = 0;
    int retour2 = 0;
    int heure;
    if(recupererDate(fichierCSV, annee, mois, jour, &heure)){
		if(fgetc(fichierCSV) != ';'){return 0;}
		int compteur = 3;

		while(compteur <= 10 && !feof(fichierCSV)){
	        switch(compteur){
	            case 6:
	                retour1 = fscanf(fichierCSV, "%f", humidite);
	                if(!retour1 || fgetc(fichierCSV) != ';'){return 0;}
	                compteur++;
	                break;
	            case 10:
	                retour2 = fscanf(fichierCSV, "%lf,%lf", latitude, longitude);
	                compteur++;
	                break;
	            default:
	                if(fgetc(fichierCSV) == ';'){
	                    compteur++;
	                }
	        }
    	}
	} else{
		return 0;
	}
    return (retour1 && (retour2 == 2));
}

int ecrireTM1(FILE* fichierCSV, ARBRE a){
	int retour1, retour2; // on déclare les variables de retour pour vérifier que tout va bien
	/* parcour infixe et écriture dans le fichier des données du noeud actuel */
	if(a!=NULL){
    	if(!ecrireTM1(fichierCSV, a->filsG)){return 0;} // on vérifie en même temps que l'ajout du fils gauche au fichier est ok
    	retour1 = fprintf(fichierCSV, "%05ld;%lf;%lf;%lf\n", a->donnee->idStation, a->donnee->temperature->temperatureMin, a->donnee->temperature->temperatureMax, (a->donnee->temperature->sommeTemperatures)/(a->donnee->temperature->nombreMesures));
    	retour2 = ecrireTM1(fichierCSV, a->filsD);
    	if(retour1==EOF || !retour2){ // on vérifie que l'ajout du noeud actuel a marché et que l'ajout du fils droit au fichier est aussi ok
			return 0;
		}
	}
	return 1; // tout s'est bien passé on retourne 1
}

/* même principe que ecrireTM1 */
int ecrireTM2(FILE* fichierCSV, ARBRE a){
	int retour1, retour2;
	if(a!=NULL){
    	if(!ecrireTM2(fichierCSV, a->filsG)){return 0;}
    	retour1 = fprintf(fichierCSV, "%d-%02d-%02dT%02d:00:00Z;%lf\n", a->donnee->date->annee, a->donnee->date->mois, a->donnee->date->jour, a->donnee->date->heure, (a->donnee->temperature->sommeTemperatures)/(a->donnee->temperature->nombreMesures));
    	retour2 = ecrireTM2(fichierCSV, a->filsD);
    	if(retour1==EOF || !retour2){
			return 0;
		}
	}
	return 1;
}

/* même principe que ecrireTM1 */
int ecrireTM3(FILE* fichierCSV, ARBRE a){
	int retour1, retour2;
	if(a!=NULL){
    	if(!ecrireTM3(fichierCSV, a->filsG)){return 0;}
    	retour1 = fprintf(fichierCSV, "%d-%02d-%02d;%02d;%lf;%05ld\n", a->donnee->date->annee, a->donnee->date->mois, a->donnee->date->jour, a->donnee->date->heure, a->donnee->temperature->sommeTemperatures, a->donnee->idStation);
    	retour2 = ecrireTM3(fichierCSV, a->filsD);
    	if(retour1==EOF || !retour2){
			return 0;
		}
	}
	return 1;
}

/* même principe que ecrireTM1 */
int ecrirePM1(FILE* fichierCSV, ARBRE a){
	int retour1, retour2;
	if(a!=NULL){
    	if(!ecrirePM1(fichierCSV, a->filsG)){return 0;}
    	retour1 = fprintf(fichierCSV, "%05ld;%lf;%lf;%lf\n", a->donnee->idStation, a->donnee->pression->pressionMin, a->donnee->pression->pressionMax, (a->donnee->pression->sommePressions)/(a->donnee->pression->nombreMesures));
    	retour2 = ecrirePM1(fichierCSV, a->filsD);
    	if(retour1==EOF || !retour2){
			return 0;
		}
	}
	return 1;
}

/* même principe que ecrireTM1 */
int ecrirePM2(FILE* fichierCSV, ARBRE a){
	int retour1, retour2;
	if(a!=NULL){
    	if(!ecrirePM2(fichierCSV, a->filsG)){return 0;}
    	retour1 = fprintf(fichierCSV, "%d-%02d-%02dT%02d:00:00Z;%lf\n", a->donnee->date->annee, a->donnee->date->mois, a->donnee->date->jour, a->donnee->date->heure, (a->donnee->pression->sommePressions)/(a->donnee->pression->nombreMesures));
    	retour2 = ecrirePM2(fichierCSV, a->filsD);
    	if(retour1==EOF || !retour2){
			return 0;
		}
	}
	return 1;
}

/* même principe que ecrireTM1 */
int ecrirePM3(FILE* fichierCSV, ARBRE a){
	int retour1, retour2;
	if(a!=NULL){
    	if(!ecrirePM3(fichierCSV, a->filsG)){return 0;}
    	retour1 = fprintf(fichierCSV, "%d-%02d-%02d;%02d;%lf;%05ld\n", a->donnee->date->annee, a->donnee->date->mois, a->donnee->date->jour, a->donnee->date->heure, a->donnee->pression->sommePressions, a->donnee->idStation);
    	retour2 = ecrirePM3(fichierCSV, a->filsD);
    	if(retour1==EOF || !retour2){
			return 0;
		}
	}
	return 1;
}

/*
	Auteur : Dorian BARRERE
	Date : 30-12-2022
	Résumer : ramène un angle censé être entre 0 et 2pi exclu dans le bon intervalle
	Entrée : l'angle
	Sortie : l'angle corrigé
*/
double garderRadian2Pi(double angle){
	if(angle < 0 || angle >= 2*PI){ // si on est inférieur strictement à 0 ou supérieur (ou égal) à 2pi on met l'angle à 0
		return 0;
	} else{
		return angle; // sinon c'est ok
	}
}

/*
	Auteur : Dorian BARRERE
	Date : 30-12-2022
	Résumer : ramène un angle censé être entre -pi exclu et pi dans le bon intervalle
	Entrée : l'angle
	Sortie : l'angle corrigé
*/
double garderRadianSymetrique(double angle){
	if(angle <= -PI || angle > PI){ // si on est inférieur (ou égal) à -pi ou supérieur strictement à pi on met l'angle à pi
		return PI;
	} else{
		return angle; // sinon c'est ok
	}
}

/*
	Auteur : Dorian BARRERE
	Date : 30-12-2022
	Résumer : ramène un angle censé être entre 0 et 360 exclu dans le bon intervalle
	Entrée : l'angle
	Sortie : l'angle corrigé
*/
double garderDegre360(double angle){
	if(angle < 0 || angle >= 360){  // si on est inférieur strictement à 0 ou supérieur (ou égal) à 360 on met l'angle à 0
		return 0;
	} else{
		return angle; // sinon c'est ok
	}
}

/*
	Auteur : Dorian BARRERE
	Date : 30-12-2022
	Résumer : récupère l'angle (dans l'intervalle [0;2pi[) des coo polaires de coo cartésiennes
	Entrée : la composante en x puis celle en y
	Sortie : l'angle trouvé
*/
double angleRadian2PiCooPolaires(double composanteX, double composanteY){
	double angle; // on déclare la variable angle
	if(composanteX == 0 && composanteY == 0){ // si les composantes sont à 0 l'angle peut être quelconque mais on met 0
		return 0;
	} else if(composanteX < 0 && composanteY == 0){ // si composante x < 0 et composante y = 0 alors l'angle est forcément pi
		return PI;
	} else{ // sinon on le calcule
		angle = 2*atan(composanteY/(composanteX+sqrt(pow(composanteX, 2)+pow(composanteY, 2)))); // formule valabre pour toutes les composantes sauf (x<= 0 && y=0)
		if(angle < 0){ // si l'angle est inférieur à 0 on le traduit en équivalent positif
			return garderRadian2Pi(2*PI + angle);
		} else{
			return garderRadian2Pi(angle);
		}
	}
}

/* même principe que ecrireTM1 */
int ecrireVent(FILE* fichierCSV, ARBRE a){
	int retour1, retour2;
	if(a!=NULL){
    	if(!ecrireVent(fichierCSV, a->filsG)){return 0;}
    	retour1 = fprintf(fichierCSV, "%05ld;%lf;%lf;%lf;%lf\n", a->donnee->idStation, a->donnee->coordonnees->latitude, a->donnee->coordonnees->longitude, garderDegre360((180/PI)*angleRadian2PiCooPolaires(a->donnee->vent->sommeComposantesX/a->donnee->vent->nombreMesures, a->donnee->vent->sommeComposantesY/a->donnee->vent->nombreMesures)), sqrt(pow(a->donnee->vent->sommeComposantesX/a->donnee->vent->nombreMesures, 2)+pow(a->donnee->vent->sommeComposantesY/a->donnee->vent->nombreMesures, 2)));
    	retour2 = ecrireVent(fichierCSV, a->filsD);
		if(retour1==EOF || !retour2){
			return 0;
		}
	}
	return 1;
}

/* même principe que ecrireTM1 */
int ecrireHauteur(FILE* fichierCSV, ARBRE a){
	int retour1, retour2;
	if(a!=NULL){
    	if(!ecrireHauteur(fichierCSV, a->filsG)){return 0;}
    	retour1 = fprintf(fichierCSV, "%lf;%lf;%lf\n", a->donnee->coordonnees->latitude, a->donnee->coordonnees->longitude, a->donnee->coordonnees->altitude);
    	retour2 = ecrireHauteur(fichierCSV, a->filsD);
    	if(retour1==EOF || !retour2){
			return 0;
		}
	}
	return 1;
}

/* même principe que ecrireTM1 */
int ecrireHumidite(FILE* fichierCSV, ARBRE a){
	int retour1, retour2;
	if(a!=NULL){
    	if(!ecrireHumidite(fichierCSV, a->filsG)){return 0;}
    	retour1 = fprintf(fichierCSV, "%lf;%lf;%f\n", a->donnee->coordonnees->latitude, a->donnee->coordonnees->longitude, a->donnee->humiditeMax);
    	retour2 = ecrireHumidite(fichierCSV, a->filsD);
    	if(retour1==EOF || !retour2){
			return 0;
		}
	}
	return 1;
}

void ligneSuitante(FILE* fichierCSV){
	fscanf(fichierCSV, "%*[^\n]\n"); // on passe simplement à la ligne suivant en lisant sans affecter tous les caractères sauf le ligne feed puis le ligne feed lui-même
}

int estMemeDate(DATE date1, DATE date2){
	return (date1.annee == date2.annee && date1.mois == date2.mois && date1.jour == date2.jour && date1.heure == date2.heure); // on vérifie simplement que c'est la même date
}

int estApres(DATE date1, DATE date2){
	if(date1.annee > date2.annee || (date1.annee == date2.annee && date1.mois > date2.mois) || (date1.annee == date2.annee && date1.mois == date2.mois && date1.jour > date2.jour) || (date1.annee == date2.annee && date1.mois == date2.mois && date1.jour == date2.jour && date1.heure > date2.heure)){ // on vérifie simplement que la date1 et ultérieure strictement à la date2
		return 1;
	} else{
		return 0;
	}
}

/*
	Auteur : Dorian BARRERE
	Date : 30-12-2022
	Résumer : transforme un angle dans l'intervalle [0;360[ dans l'intervalle ]-pi;pi]
	Entrée : l'angle dans l'intervalle [0;360[
	Sortie : l'angle dans l'intervalle ]-pi;pi]
*/
double degre360EnRadianSymetrique(double angleDegre360){
	/* on transforme un angle en degrés en radians */
	if(angleDegre360 > 180){ // si l'angle est supérieur à 180 degrés on pense à le ramener avant dans l'intervalle ]-180;180]
		return garderRadianSymetrique((PI/180)*(angleDegre360-360));
	} else{
		return garderRadianSymetrique((PI/180)*angleDegre360);
	}
}

int appartientAuFiltre(FILTRE filtre, double latitude, double longitude, int annee, int mois, int jour){
	if(filtre.latitude.actif && (latitude < filtre.latitude.latitudeMin || latitude > filtre.latitude.latitudeMax)){ // si le filtre latitude est actif on vérifie que les données passent
		return 0;
	}

	if(filtre.longitude.actif && (longitude < filtre.longitude.longitudeMin || longitude > filtre.longitude.longitudeMax)){ // si le filtre longitude est actif on vérifie que les données passent
		return 0;
	}

	/* on créé une structure date avec les valeurs données en paramètres */
	DATE date;
	date.annee = annee;
	date.mois = mois;
	date.jour = jour;
	date.heure = 0;
	if(filtre.date.actif && ((!estApres(date, filtre.date.dateMin) && !estMemeDate(date, filtre.date.dateMin)) || estApres(date, filtre.date.dateMax))){ // si le filtre date est actif on vérifie que les données passent
		return 0;
	}
	return 1;
}

FILTRE* initialiserFiltre(){
	FILTRE* filtre = malloc(sizeof(FILTRE)); // on l'alloue
	/* on met tout à 0 */
	filtre->latitude.actif = 0;
	filtre->latitude.latitudeMin = 0;
	filtre->latitude.latitudeMax = 0;
	filtre->longitude.actif = 0;
	filtre->longitude.longitudeMin = 0;
	filtre->longitude.longitudeMax = 0;
	filtre->date.actif = 0;
	filtre->date.dateMin.annee = 0;
	filtre->date.dateMin.mois = 0;
	filtre->date.dateMin.jour = 0;
	filtre->date.dateMin.heure = 0;
	filtre->date.dateMax.annee = 0;
	filtre->date.dateMax.mois = 0;
	filtre->date.dateMax.jour = 0;
	filtre->date.dateMax.heure = 0;
	return filtre;
}

int estValide(DATE date) {
	if (date.annee >= 1970) { // année supérieure à 1970
		if (date.heure >= 0 && date.heure < 24) { // heure comprise entre 0 et 23
			if (date.jour > 0) { // pas de jour négatif ou nul
				if (date.mois == 1 || date.mois == 3 || date.mois == 5 || date.mois == 7 || date.mois == 8 || date.mois == 10 || date.mois == 12) { // mois à 31 jours
					if(date.jour <= 31) { // nb de jours cohérents
						return 1;
					} else { // nb de jours non cohérents
						return 0;
					}
				} else if (date.mois == 4 || date.mois == 6 || date.mois == 9 || date.mois == 11){ // mois à 30 jours
					if(date.jour <= 30) { // nb de jours cohérents
						return 1;
					} else { // nb de jours non cohérents
						return 0;
					}
				} else if(date.mois == 2) { //cas du mois de février
					if ((((date.annee % 4) == 0) && ((date.annee % 100) != 0)) || ((date.annee % 400) == 0)) { // année bissextile
						if (date.jour <= 29) {
							return 1;// nb de jours cohérents
						} else {
							return 0;// nb de jours non cohérents
						}
					} else { // année non bissextile
						if (date.jour <= 28) { // nb de jours cohérents
							return 1;
						} else { // nb de jours non cohérents
							return 0;
						}
					}
				} else { // mois en dehors de la plage 1-12
					return 0;
				}
			} else { // jour négatif ou nul
				return 0;
			}
		} else { // heure erronée
			return 0;
		}
	} else { // annee négative
		return 0;
	}
}

int estDossier(char* chemin){
	DIR* ouverturePotentielDossier = opendir(chemin);
	if(ouverturePotentielDossier!=NULL){
		closedir(ouverturePotentielDossier);
		return 1;
	} else{
		return 0;
	}
}