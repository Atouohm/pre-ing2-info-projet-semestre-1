/* on inclu tout ce dont à a besoin */
#include "librairiesBasiques.h"
#include "utilitaires.h"
#include "arbres.h"
#define MAX(a,b) (a>b?a:b) // macro qui donne le maximum entre 2 nombres
#define MIN(a,b) (a<b?a:b) // macro qui donne le minimum entre 2 nombres

/*
	Auteur : Dorian BARRERE
	Date : 24-10-2022
	Résumer : créé un élément de l'arbre en l'allouant et en lui mettant la bonne donnée
	Entrée : la hauteur, le pointeur vers la donnée, l'adresse du fils gauche, l'adresse du fils droit
	Sortie : renvoie l'adresse du noeud
*/
NOEUD* creerNoeud(int hauteur, DONNEE* donnee, NOEUD* filsG, NOEUD* filsD){
	/* on alloue un nouveau noeud et on lui met la bonne donnée */
	NOEUD* noeudTemp = malloc(sizeof(NOEUD));
	noeudTemp->hauteur = hauteur;
	noeudTemp->donnee = donnee;
	noeudTemp->filsG = filsG;
	noeudTemp->filsD = filsD;
	return noeudTemp;
}

/*
	Auteur : Dorian BARRERE
	Date : 24-10-2022
	Résumer : renvoie la hauteur d'une arbre avl
	Entrée : l'arbre
	Sortie : la hauteur si l'arbre était non vide, -1 s'il l'était
*/
int hauteurAVL(ARBRE a){
	if(a==NULL){ // s'il est vide
		return -1;
	} else{
		return a->hauteur; // sinon on met directement sa hauteur
	}
}

/*
	Auteur : Dorian BARRERE
	Date : 24-10-2022
	Résumer : fait une rotation gauche sur la racine de l'arbre entré
	Entrée : un pointeur de noeud ie un arbre
	Sortie : l'arbre modifié
*/
ARBRE rotationGauche(ARBRE a){
	NOEUD* noeudTemp = a->filsD;
	a = creerNoeud(MAX(hauteurAVL(a->filsG), hauteurAVL(noeudTemp->filsG))+1, a->donnee, a->filsG, noeudTemp->filsG);
	return creerNoeud(MAX(hauteurAVL(a), hauteurAVL(noeudTemp->filsD))+1, noeudTemp->donnee, a, noeudTemp->filsD);
}

/*
	Auteur : Dorian BARRERE
	Date : 24-10-2022
	Résumer : fait une rotation droite sur la racine de l'arbre entré
	Entrée : un pointeur de noeud ie un arbre
	Sortie : l'arbre modifié
*/
ARBRE rotationDroite(ARBRE a){
	NOEUD* noeudTemp = a->filsG;
	a = creerNoeud(MAX(hauteurAVL(noeudTemp->filsD), hauteurAVL(a->filsD))+1, a->donnee, noeudTemp->filsD, a->filsD);
	return creerNoeud(MAX(hauteurAVL(noeudTemp->filsG), hauteurAVL(a))+1, noeudTemp->donnee, noeudTemp->filsG, a);
}

/*
	Auteur : Dorian BARRERE
	Date : 24-10-2022
	Résumer : fait une rotation gauche sur me fils gauche puis une rotation droite sur la racine de l'arbre entré
	Entrée : un pointeur de noeud ie un arbre
	Sortie : l'arbre modifié
*/
ARBRE rotationGaucheDroite(ARBRE a){
	a->filsG = rotationGauche(a->filsG);
	return rotationDroite(a);
}

/*
	Auteur : Dorian BARRERE
	Date : 24-10-2022
	Résumer : fait une rotation droite sur me fils droite puis une rotation gauche sur la racine de l'arbre entré
	Entrée : un pointeur de noeud ie un arbre
	Sortie : l'arbre modifié
*/
ARBRE rotationDroiteGauche(ARBRE a){
	a->filsD = rotationDroite(a->filsD);
	return rotationGauche(a);
}

/*
	Auteur : Dorian BARRERE
	Date : 24-10-2022
	Résumer : calcule l'écart de hauteur entre le fils gauche et droit d'un noeud (il faut mettre en paramètre un pointeur de noeud)
	Entrée : l'arbre
	Sortie : l'écart
*/
int ecart(ARBRE a){
	if(a==NULL){ // s'il est nul pas d'écart
		return 0;
	} else{
		return hauteurAVL(a->filsD)-hauteurAVL(a->filsG); // sinon on le calcule
	}
}

/*
	Auteur : Dorian BARRERE
	Date : 24-10-2022
	Résumer : rééquilibre un arbre si nécessaire
	Entrée : l'arbre que l'on veut rééquilibré ie un pointeur de noeud
	Sortie : l'arbre de base ou rééquilibré si c'était nécessaire
*/
ARBRE equilibrage(ARBRE a){
	/* tous les cas de déséquilibre possibles après insertion de 1 noeud sont traités */
	if(ecart(a)==-2){
		if(ecart(a->filsG)==-1 || ecart(a->filsG)==0){
			a = rotationDroite(a);
		} else{
			a = rotationGaucheDroite(a);
		}
	} else if(ecart(a)==2){
		if(ecart(a->filsD)==1 || ecart(a->filsD)==0){
			a = rotationGauche(a);
		} else{
			a = rotationDroiteGauche(a);
		}
	} else{
		a->hauteur = MAX(hauteurAVL(a->filsG), hauteurAVL(a->filsD))+1;
	}
	return a;
}

ARBRE insertionTM1(DONNEE* donnee, ARBRE a){
	if(a==NULL){
		return creerNoeud(0, donnee, NULL, NULL);
	} else{
		if(donnee->idStation == a->donnee->idStation){ // si on est sur la même station
			/* on récupère la donnée mais on rajoute pas un noeud à l'arbre */
			a->donnee->temperature->sommeTemperatures = a->donnee->temperature->sommeTemperatures + donnee->temperature->sommeTemperatures;
			a->donnee->temperature->nombreMesures++;
			a->donnee->temperature->temperatureMin = MIN(a->donnee->temperature->temperatureMin, donnee->temperature->temperatureMin);
			a->donnee->temperature->temperatureMax = MAX(a->donnee->temperature->temperatureMax, donnee->temperature->temperatureMax);
			free(donnee->temperature);
			free(donnee);
		} else if(donnee->idStation < a->donnee->idStation){ // sinon si l'id est strictement inférieur on insère à gauche
			a->filsG = insertionTM1(donnee, a->filsG);
		} else{ // sinon à droite
			a->filsD = insertionTM1(donnee, a->filsD);
		}
	}
	a = equilibrage(a); // on vérifie l'équilibrage
	return a;
}

ARBRE insertionTM2(DONNEE* donnee, ARBRE a){
	if(a==NULL){
		return creerNoeud(0, donnee, NULL, NULL);
	} else{
		if(estMemeDate(*(donnee->date), *(a->donnee->date))){ // si même date et récupère la donnée mais on rajoute pas un noeud à l'arbre */
			a->donnee->temperature->sommeTemperatures = a->donnee->temperature->sommeTemperatures + donnee->temperature->sommeTemperatures;
			a->donnee->temperature->nombreMesures++;
			free(donnee->temperature);
			free(donnee->date);
			free(donnee);
		} else if(estApres(*(donnee->date), *(a->donnee->date))){ // si la date est ultérieure on insère à droite
			a->filsD = insertionTM2(donnee, a->filsD);
		} else{
			a->filsG = insertionTM2(donnee, a->filsG); // sinon à gauche
		}
	}
	a = equilibrage(a); // on vérifie l'équilibrage
	return a;
}

ARBRE insertionTM3(DONNEE* donnee, ARBRE a){
	if(a==NULL){
		return creerNoeud(0, donnee, NULL, NULL);
	} else{
		if(estMemeDate(*(donnee->date), *(a->donnee->date))){ // même principe que insertionTM2 sauf que ici si c'est la même date on tri selon l'id croissant de station
			if(donnee->idStation < a->donnee->idStation){
				a->filsG = insertionTM3(donnee, a->filsG);
			} else{
				a->filsD = insertionTM3(donnee, a->filsD);
			}
		} else if(estApres(*(donnee->date), *(a->donnee->date))){
			a->filsD = insertionTM3(donnee, a->filsD);
		} else{
			a->filsG = insertionTM3(donnee, a->filsG);
		}
	}
	a = equilibrage(a);
	return a;
}

/* équivalent à insertionTM1 sauf que la donnée c'est la pression */
ARBRE insertionPM1(DONNEE* donnee, ARBRE a){
	if(a==NULL){
		return creerNoeud(0, donnee, NULL, NULL);
	} else{
		if(donnee->idStation == a->donnee->idStation){
			a->donnee->pression->sommePressions = a->donnee->pression->sommePressions + donnee->pression->sommePressions;
			a->donnee->pression->nombreMesures++;
			a->donnee->pression->pressionMin = MIN(a->donnee->pression->pressionMin, donnee->pression->pressionMin);
			a->donnee->pression->pressionMax = MAX(a->donnee->pression->pressionMax, donnee->pression->pressionMax);
			free(donnee->pression);
			free(donnee);
		} else if(donnee->idStation < a->donnee->idStation){
			a->filsG = insertionPM1(donnee, a->filsG);
		} else{
			a->filsD = insertionPM1(donnee, a->filsD);
		}
	}
	a = equilibrage(a);
	return a;
}

/* équivalent à insertionTM2 sauf que la donnée c'est la pression */
ARBRE insertionPM2(DONNEE* donnee, ARBRE a){
	if(a==NULL){
		return creerNoeud(0, donnee, NULL, NULL);
	} else{
		if(estMemeDate(*(donnee->date), *(a->donnee->date))){
			a->donnee->pression->sommePressions = a->donnee->pression->sommePressions + donnee->pression->sommePressions;
			a->donnee->pression->nombreMesures++;
			free(donnee->pression);
			free(donnee->date);
			free(donnee);
		} else if(estApres(*(donnee->date), *(a->donnee->date))){
			a->filsD = insertionPM2(donnee, a->filsD);
		} else{
			a->filsG = insertionPM2(donnee, a->filsG);
		}
	}
	a = equilibrage(a);
	return a;
}

/* strictement équivalent à insertionTM3 */
ARBRE insertionPM3(DONNEE* donnee, ARBRE a){
	return insertionTM3(donnee, a);
}

/* même principe que insertionTM1 */
ARBRE insertionVent(DONNEE* donnee, ARBRE a){
	if(a==NULL){
		return creerNoeud(0, donnee, NULL, NULL);
	} else{
		if(donnee->idStation == a->donnee->idStation){ // si on est sur la même station on récupère la donnée mais on rajoute pas un noeud à l'arbre
			a->donnee->vent->sommeComposantesX = a->donnee->vent->sommeComposantesX + donnee->vent->sommeComposantesX;
			a->donnee->vent->sommeComposantesY = a->donnee->vent->sommeComposantesY + donnee->vent->sommeComposantesY;
			a->donnee->vent->nombreMesures++;
			free(donnee->coordonnees);
			free(donnee->vent);
			free(donnee);
		} else if(donnee->idStation < a->donnee->idStation){
			a->filsG = insertionVent(donnee, a->filsG);
		} else{
			a->filsD = insertionVent(donnee, a->filsD);
		}
	}
	a = equilibrage(a);
	return a;
}

int stationDejaPresente(ARBRE a, double latitude, double longitude, double altitude){
    if(a==NULL){ // si l'arbre est null c pas bon
        return 0;
    } else if(latitude == a->donnee->coordonnees->latitude && longitude == a->donnee->coordonnees->longitude){ // si on est sur un noeud qui a les même coo alors déjà présent
        return 1;
    } else if(altitude == a->donnee->coordonnees->altitude){ // si l'altitude est égale on doit vérifier à droite et à gauche car le rééquilibrage qui a créé l'arbre ne conserve qui l'ordre strict
        return stationDejaPresente(a->filsG, latitude, longitude, altitude) || stationDejaPresente(a->filsD, latitude, longitude, altitude);
    } else if(altitude > a->donnee->coordonnees->altitude){ // si c'est strictement supérieur par contre c'est forcément à gauche
        return stationDejaPresente(a->filsG, latitude, longitude, altitude); // si c'est strictement inférieur par contre c'est forcément à droite
    } else{
        return stationDejaPresente(a->filsD, latitude, longitude, altitude);
    }
}

ARBRE insertionHauteur(DONNEE* donnee, ARBRE a){
    if(a==NULL){
        return creerNoeud(0, donnee, NULL, NULL);
    } else{
        if(donnee->coordonnees->altitude > a->donnee->coordonnees->altitude){ // si altitude strictement supérieure on insère à gauche
            a->filsG = insertionHauteur(donnee, a->filsG);
        } else{ // sinon l'inverse
            a->filsD = insertionHauteur(donnee, a->filsD);
        }
    }
    a = equilibrage(a);
    return a;
}

ARBRE insertionHumidite(DONNEE* donnee, ARBRE a){
    if(a==NULL){
		return creerNoeud(0, donnee, NULL, NULL);
	} else{
		if(donnee->idStation == a->donnee->idStation){ // si c'est la même station on prend le max mais on insère pas un nouveau noeud
			a->donnee->humiditeMax = MAX(a->donnee->humiditeMax, donnee->humiditeMax);
			free(donnee->coordonnees);
			free(donnee);
		} else if(donnee->idStation < a->donnee->idStation){ // si inférieur on insère à droite
			a->filsD = insertionHumidite(donnee, a->filsD);
		} else{ // sinon à gauche
			a->filsG = insertionHumidite(donnee, a->filsG);
		}
	}
	a = equilibrage(a);
	return a;
}

/*
	Auteur : Dorian BARRERE
	Date : 31-12-2022
	Résumer : insère une donnée dans un arbre pour le générateur Humidité mais en triant par humidités décroissantes
	Entrée : le pointeur de la donnée, l'arbre
	Sortie : l'arbre contenant la nouvelle donnée
*/
ARBRE insertionHumidite2(DONNEE* donnee, ARBRE a){
	if(a==NULL){
		return creerNoeud(0, donnee, NULL, NULL);
	} else{
		if(donnee->humiditeMax <= a->donnee->humiditeMax){ // si inférieur ou égal on insère à droite
			a->filsD = insertionHumidite2(donnee, a->filsD);
		} else{ // sinon à gauche
			a->filsG = insertionHumidite2(donnee, a->filsG);
		}
	}
	a = equilibrage(a);
	return a;
}

/*
	Auteur : Dorian BARRERE
	Date : 31-12-2022
	Résumer : insère donnée par donnée dans un nouvel arbre en triant par humidités décrossantes
	Entrée : l'arbre non trié, un pointeur vers un arbre vierge
	Sortie : rien
*/
void trierHumiditeCachee(ARBRE arbreNonTrie, ARBRE* arbreTrie){
	if(arbreNonTrie!=NULL){
		trierHumiditeCachee(arbreNonTrie->filsG, arbreTrie); // on tri l'arbre que représente le fils gauche
		trierHumiditeCachee(arbreNonTrie->filsD, arbreTrie); // on tri l'arbre que représente le fils droit
		*arbreTrie = insertionHumidite2(arbreNonTrie->donnee, *arbreTrie); // on insère le noeud dans un nouvel arbre selon un tri par humidités décroissantes
    	free(arbreNonTrie); // on libère la place réservée que pointe le pointeur de noeud (qui n'a ni fils gauche ni fils droit) que représente ici arbreNonTrie, tout en laissant intacte la place réservée que pointait le pointeur arbreNonTrie->donnee (maintenant c'est un noeud de l'arbreTrie qui a l'adresse et qui la pointe)
	}
}

/*
	Auteur : Dorian BARRERE
	Date : 31-12-2022
	Résumer : tri un arbre par humidités décroissantes
	Entrée : le pointeur vers l'arbre non trié
	Sortie : rien
*/
void trierHumidite(ARBRE* arbreNonTrie){
	ARBRE arbreTrie = NULL; // on déclare le nouvel arbre
	trierHumiditeCachee(*arbreNonTrie, &arbreTrie); // on le rempli de l'arbre placé en paramètre mais en le triant par humidités décroissantes
	*arbreNonTrie = arbreTrie; // on retourne l'arbre trié (l'ancien étant vide)
}

ARBRE viderArbreTM1(ARBRE a){
	if(a!=NULL){
		a->filsG = viderArbreTM1(a->filsG); // on vide le gauche
    	a->filsD = viderArbreTM1(a->filsD); // on vide le droit
    	/* on libère le noeud */
   		free(a->donnee->temperature);
   		free(a->donnee);
    	free(a);
	}
	return NULL;
}

ARBRE viderArbreTM2(ARBRE a){
	if(a!=NULL){
		a->filsG = viderArbreTM2(a->filsG); // on vide le gauche
    	a->filsD = viderArbreTM2(a->filsD); // on vide le droit
    	/* on libère le noeud */
   		free(a->donnee->temperature);
   		free(a->donnee->date);
   		free(a->donnee);
    	free(a);
	}
	return NULL;
}

ARBRE viderArbreTM3(ARBRE a){
	return viderArbreTM2(a);
}

ARBRE viderArbrePM1(ARBRE a){
	if(a!=NULL){
		a->filsG = viderArbrePM1(a->filsG); // on vide le gauche
    	a->filsD = viderArbrePM1(a->filsD); // on vide le droit
   		free(a->donnee->pression);
   		free(a->donnee);
    	free(a);
    	/* on libère le noeud */
	}
	return NULL;
}

ARBRE viderArbrePM2(ARBRE a){
	if(a!=NULL){
		a->filsG = viderArbrePM2(a->filsG); // on vide le gauche
    	a->filsD = viderArbrePM2(a->filsD); // on vide le droit
   		free(a->donnee->pression);
   		free(a->donnee->date);
   		free(a->donnee);
    	free(a);
    	/* on libère le noeud */
	}
	return NULL;
}

ARBRE viderArbrePM3(ARBRE a){
	return viderArbrePM2(a);
}

ARBRE viderArbreVent(ARBRE a){
	if(a!=NULL){
		a->filsG = viderArbreVent(a->filsG); // on vide le gauche
    	a->filsD = viderArbreVent(a->filsD); // on vide le droit
   		free(a->donnee->coordonnees);
   		free(a->donnee->vent);
   		free(a->donnee);
   		/* on libère le noeud */
    	free(a);
	}
	return NULL;
}

ARBRE viderArbreHauteur(ARBRE a){
	if(a!=NULL){
		a->filsG = viderArbreHauteur(a->filsG); // on vide le gauche
    	a->filsD = viderArbreHauteur(a->filsD);
   		free(a->donnee->coordonnees);
   		free(a->donnee);
    	free(a);
    	/* on libère le noeud */
	}
	return NULL;
}

ARBRE viderArbreHumidite(ARBRE a){
	return viderArbreHauteur(a);
}