#!/bin/bash

function erreurScriptGnuplot(){ # automatise la gestion d'une erreur dans l'utilisation d'un script gnuplot
	printf "\n-!- erreur dans l'utilisation du script gnuplot -!-\n"
	exit 1
}

function erreurExecutable(){  # automatise la gestion d'une erreur dans la mise en place de l'exécutable
	printf "\n-!- erreur dans la mise en place de l'exécutable -!-\n"
	exit 1
}

if ! [ -x "./application/executable" ] # mais avant tout on vérifie que l'exécutable est présent et exécutable
then # sinon
	cd ./application
	if (( $? != 0 )); then erreurExecutable; fi
	make exe 2> /dev/null # on le crée
	if (( $? != 0 )); then erreurExecutable; fi
	chmod u+x ./executable 2> /dev/null >> /dev/null # et on donne les droits à l'utilisateur de l'exécuter
	if (( $? != 0 )); then erreurExecutable; fi
	cd ../
fi

function erreurCommande(){ # fonction qui automatique la gestion d'une erreur dans la commande (termine le script avec une erreur)
	./application/executable erreurVoulue
	exit $?
}

function aideCommande(){ # fonction qui automatique l'affichage de l'aide (termine le script sans erreur)
	./application/executable --help
	exit $?
}

if (($# == 0)) # si il n'y a aucun argument on peut déjà dire que c'est pas bon
then
	erreurCommande
fi

## Gestion de la commande entrée (de la ligne 39 à la ligne 186) ##

Fdrapeau=0 # un drapeau permet de savoir si une option a été rentré (1) ou non (0)
Gdrapeau=0
Sdrapeau=0
Adrapeau=0
Odrapeau=0
Qdrapeau=0

t1Drapeau=0
t2Drapeau=0
t3Drapeau=0
p1Drapeau=0
p2Drapeau=0
p3Drapeau=0
wDrapeau=0
hDrapeau=0
mDrapeau=0
gDrapeau=0
aDrapeau=0
dDrapeau=0
iDrapeau=0
iValeur=""
oDrapeau=0
oValeur="meteo"

commande="" # variable qui va stocker les options (et leurs arguments) "options" communes à toutes les commandes si l'on doit en faire plusieurs (excepté le -i qui est traité comme le -o pour être compatible avec les adresses/noms des fichiers d'entrés contenant des espaces)
indiceArgument=1 # pour pouvoir suivre notre avancée dans la commande
while (( $indiceArgument <= $# )) # boucle qui parcourt tout les arguments du shell
do
	case ${!indiceArgument} in # à chaque fois on regarde qu'elle est la valeur de l'argument

		"-F") # si c'est -F on regarde si l'argument suivant est bien une option (qu'elle existe ou non) pour éviter que le -F soit entier une option et son argument potentiel
			((indiceArgument++)) # on avance de 1 indice pour regarder le prochain argument
			if ( (( $indiceArgument <= $# )) && [[ !(${!indiceArgument} =~ ^-[a-zA-Z] || ${!indiceArgument} =~ ^--[a-zA-Z]) ]] ) || (( Fdrapeau == 1 || Gdrapeau == 1 || Sdrapeau == 1 || Adrapeau == 1 || Odrapeau == 1 || Qdrapeau == 1 || gDrapeau == 1 || aDrapeau == 1)); then erreurCommande; fi # on vérifie que tout est bon (que l'option n'est pas entre une autre option et son argument, et qu'il n'y a pas déjà une autre option exclusive shell OU un -g ou un -a qui ne sont pas compatibles avec les options shell). Si ce n'est pas le cas alors on appelle la fonction erreurCommande pour terminer le script en affichant l'aide
			Fdrapeau=1 # on dit qu'on l'a croisé
			((indiceArgument--));; # on revient là où on en était

		"-G") # strictement équivalent à -F
			((indiceArgument++))
			if ( (( $indiceArgument <= $# )) && [[ !(${!indiceArgument} =~ ^-[a-zA-Z] || ${!indiceArgument} =~ ^--[a-zA-Z]) ]] ) || (( Fdrapeau == 1 || Gdrapeau == 1 || Sdrapeau == 1 || Adrapeau == 1 || Odrapeau == 1 || Qdrapeau == 1 || gDrapeau == 1 || aDrapeau == 1)); then erreurCommande; fi
			Gdrapeau=1
			((indiceArgument--));;

		"-S") # strictement équivalent à -F
			((indiceArgument++))
			if ( (( $indiceArgument <= $# )) && [[ !(${!indiceArgument} =~ ^-[a-zA-Z] || ${!indiceArgument} =~ ^--[a-zA-Z]) ]] ) || (( Fdrapeau == 1 || Gdrapeau == 1 || Sdrapeau == 1 || Adrapeau == 1 || Odrapeau == 1 || Qdrapeau == 1 || gDrapeau == 1 || aDrapeau == 1)); then erreurCommande; fi
			Sdrapeau=1
			((indiceArgument--));;

		"-A") # strictement équivalent à -F
			((indiceArgument++))
			if ( (( $indiceArgument <= $# )) && [[ !(${!indiceArgument} =~ ^-[a-zA-Z] || ${!indiceArgument} =~ ^--[a-zA-Z]) ]] ) || (( Fdrapeau == 1 || Gdrapeau == 1 || Sdrapeau == 1 || Adrapeau == 1 || Odrapeau == 1 || Qdrapeau == 1 || gDrapeau == 1 || aDrapeau == 1)); then erreurCommande; fi
			Adrapeau=1
			((indiceArgument--));;

		"-O") # strictement équivalent à -F
			((indiceArgument++))
			if ( (( $indiceArgument <= $# )) && [[ !(${!indiceArgument} =~ ^-[a-zA-Z] || ${!indiceArgument} =~ ^--[a-zA-Z]) ]] ) || (( Fdrapeau == 1 || Gdrapeau == 1 || Sdrapeau == 1 || Adrapeau == 1 || Odrapeau == 1 || Qdrapeau == 1 || gDrapeau == 1 || aDrapeau == 1)); then erreurCommande; fi
			Odrapeau=1
			((indiceArgument--));;

		"-Q") # strictement équivalent à -F
			((indiceArgument++))
			if ( (( $indiceArgument <= $# )) && [[ !(${!indiceArgument} =~ ^-[a-zA-Z] || ${!indiceArgument} =~ ^--[a-zA-Z]) ]] ) || (( Fdrapeau == 1 || Gdrapeau == 1 || Sdrapeau == 1 || Adrapeau == 1 || Odrapeau == 1 || Qdrapeau == 1 || gDrapeau == 1 || aDrapeau == 1)); then erreurCommande; fi
			Qdrapeau=1
			((indiceArgument--));;

		"-t")
			((indiceArgument++)) # on avance de 1 indice pour regarder le prochain argument
			case ${!indiceArgument} in # suivant les cas on voir ce qu'on fait
				'1') # dans ce cas c'est ok il y avait -t 1
					if (( t1Drapeau == 1 )); then erreurCommande; fi; # on vérifie quand même que ce n'est pas un double
					t1Drapeau=1;; # on dit qu'on l'a croisé
				'2') # pareil que le cas précédent
					if (( t2Drapeau == 1 )); then erreurCommande; fi;
					t2Drapeau=1;;
				'3') # pareil que le cas précédent
					if (( t3Drapeau == 1 )); then erreurCommande; fi;
					t3Drapeau=1;;
				*) erreurCommande # si c'est un autre cas c'est pas bon
			esac;;

		"-p") # strictement équivalent à -t
			((indiceArgument++))
			case ${!indiceArgument} in
				'1')
					if (( p1Drapeau == 1 )); then erreurCommande; fi;
					p1Drapeau=1;;
				'2')
					if (( p2Drapeau == 1 )); then erreurCommande; fi;
					p2Drapeau=1;;
				'3')
					if (( p3Drapeau == 1 )); then erreurCommande; fi;
					p3Drapeau=1;;
				*) erreurCommande
			esac;;

		"-w")
			if (( wDrapeau == 1 )); then erreurCommande; fi; # on vérifie que ce n'est pas en double
			wDrapeau=1;; # on dit qu'on l'a croisé

		"-h") # strictement équivalent à -w
			if (( hDrapeau == 1 )); then erreurCommande; fi;
			hDrapeau=1;;

		"-m") # strictement équivalent à -w
			if (( mDrapeau == 1 )); then erreurCommande; fi;
			mDrapeau=1;;

		"-g")
			if (( gDrapeau == 1 || $indiceArgument+2 > $# || Fdrapeau == 1 || Gdrapeau == 1 || Sdrapeau == 1 || Adrapeau == 1 || Odrapeau == 1 || Qdrapeau == 1 )); then erreurCommande; fi; # on regarde qu'il y a bien 2 arguments qui suivent et que l'on n'a pas déjà croisé l'option
			gDrapeau=1 # on dit qu'on l'a croisé
			((indiceArgument++))
			commande="$commande -g ${!indiceArgument}" # on récupère le premier argument et on l'ajoute à la commande (même s'il n'est pas bon car c'est le programme c qui le vérifiera)
			((indiceArgument++))
			commande="$commande ${!indiceArgument}";; # on récupère le premier argument et on l'ajoute à la commande

		"-a") # strictement équivalent à -a
			if (( aDrapeau == 1 || $indiceArgument+2 > $# || Fdrapeau == 1 || Gdrapeau == 1 || Sdrapeau == 1 || Adrapeau == 1 || Odrapeau == 1 || Qdrapeau == 1 )); then erreurCommande; fi;
			aDrapeau=1
			((indiceArgument++))
			commande="$commande -a ${!indiceArgument}"
			((indiceArgument++))
			commande="$commande ${!indiceArgument}";;

		"-d") # strictement équivalent à -d
			if (( dDrapeau == 1 || $indiceArgument+2 > $# )); then erreurCommande; fi;
			dDrapeau=1
			((indiceArgument++))
			commande="$commande -d ${!indiceArgument}"
			((indiceArgument++))
			commande="$commande ${!indiceArgument}";;
		"-i") # l'idée est de récupèrer la valeur de l'argument mais de ne rien ajouter à la commande. Même si le -i (et son argument aussi ou non) n'était pas bien placé (entre une option et son argument potentiel) soit le script soit le c retournera une erreur pendant l'analyse de la commande (dans tous les cas)
			if (( iDrapeau == 1 || $indiceArgument+1 > $# )); then erreurCommande; fi;
			iDrapeau=1
			((indiceArgument++))
			iValeur="${!indiceArgument}";;
		"-o") # strictement équivalent à -i
			if (( oDrapeau == 1 || $indiceArgument+1 > $# )); then erreurCommande; fi;
			oDrapeau=1
			((indiceArgument++))
			oValeur="${!indiceArgument}";;
		"--help") aideCommande;; # si c'est --help on appelle la fonction qui automatise l'affichage de l'aide (et qui termine le script)
		*) erreurCommande # sinon erreur
	esac
	((indiceArgument++)) # on passe à l'argument suivant et on refait ça
done

## On regarde s'il y a une option excluse shell et si c'est le cas on met à jour la commande comme il faut ##

if (( Fdrapeau == 1 ))
then
	commande="$commande -g -5.38 9.88 -a 41.32 51.27"
elif (( Gdrapeau == 1 ))
then
	commande="$commande -g -55 -51 -a 1.86 6.2"
elif (( Sdrapeau == 1 ))
then
	commande="$commande -g -60 -52 -a 46.30 52.2"
elif (( Adrapeau == 1 ))
then
	commande="$commande -g -63.7 -60.4 -a 11.80 18.5"
elif (( Odrapeau == 1 ))
then
	commande="$commande -g 35 112 -a -60 0"
elif (( Qdrapeau == 1 ))
then
	commande="$commande -g -180 180 -a -90 -60"
fi

## On gère le nom du fichier de sortie suivant les différents cas ##

indiceFichier=1; # pour savoir le nom du fichier de sortie si on doit en générer plusieurs
nombreFichiersAGenerer=$(($t1Drapeau + $t2Drapeau + $t3Drapeau + $p1Drapeau + $p2Drapeau + $p3Drapeau + $wDrapeau + $hDrapeau + $mDrapeau)) # on récupère le nombre de fichier que l'on doit générer
if (( nombreFichiersAGenerer == 0 )) || [[ $oValeur = "" ]] # s'il y en a 0 c'est qu'il y a un soucis, y compris si le nom demandé est le nom "vide"
then
	erreurCommande
elif (( nombreFichiersAGenerer == 1 && oDrapeau == 1 )) # s'il y en a que 1 seul on met le bon nom au fichier de sortie
then
	nomFichierSortie="${oValeur}"
else # sinon on le met comme il faut pour en générer plusieurs (sachant que le cas où il n'y a pas de -o fait partie de ce else, puisque le nom du fichier sera alors meteo000X)
	nomFichierSortie="${oValeur}000${indiceFichier}"
fi

## Maintenant on va générer les fichiers de données puis les graphiques suivant ce qu'a demandé l'utilisateur ##

if (( t1Drapeau == 1 )) # s'il y a l'opion -t 1
then
	./application/executable $commande -t 1 -i "$iValeur" -o "./fichiersGeneres/$nomFichierSortie" # on génère le fichier de données
	if (( $? != 0 )); then exit $?; fi # on vérifie que tout s'est bien passé
	((nombreFichiersAGenerer--)) # on vient de faire une génération donc on réduit de 1 le nombre des fichiers qu'il reste à générer
	
	printf "Génération du diagramme \"./fichiersGeneres/$nomFichierSortie.png\" pour la température en mode 1...\n" # on affiche ce que l'on fait ie créer le diagramme
	if ! [ -x "./application/scriptsGnuplot/tm1.sh" ] # on vérifie qu'on peut exécuter le script
	then # sinon on se donne les droits
		chmod u+x ./application/scriptsGnuplot/tm1.sh 2> /dev/null >> /dev/null
		if (( $? != 0 )); then erreurScriptGnuplot; fi # on vérifie que tout c'est bien passé
	fi
	./application/scriptsGnuplot/tm1.sh "./fichiersGeneres/$nomFichierSortie.dat" "./fichiersGeneres/$nomFichierSortie.png" 2> /dev/null >> /dev/null # on appelle le script pour générer le diagramme
	if (( $? != 0 )); then erreurScriptGnuplot; fi # et on vérifie que tout c'est bien passé

	if (( nombreFichiersAGenerer > 0 )) # s'il en reste au moins 1 à générer
	then # alors on prépare le nom du prochain fichier que l'on va générer
		((indiceFichier++));
		nomFichierSortie="${oValeur}000${indiceFichier}"
	fi
fi

if (( t2Drapeau == 1 )) # strictement le même principe que pour -t 1
then
	./application/executable $commande -t 2 -i "$iValeur" -o "./fichiersGeneres/$nomFichierSortie"
	if (( $? != 0 )); then exit $?; fi
	((nombreFichiersAGenerer--))
	
	printf "Génération du diagramme \"./fichiersGeneres/$nomFichierSortie.png\" pour la température en mode 2...\n"
	if ! [ -x "./application/scriptsGnuplot/tm2.sh" ]
	then
		chmod u+x ./application/scriptsGnuplot/tm2.sh 2> /dev/null >> /dev/null
		if (( $? != 0 )); then erreurScriptGnuplot; fi
	fi
	./application/scriptsGnuplot/tm2.sh "./fichiersGeneres/$nomFichierSortie.dat" "./fichiersGeneres/$nomFichierSortie.png" 2> /dev/null >> /dev/null
	if (( $? != 0 )); then erreurScriptGnuplot; fi

	if (( nombreFichiersAGenerer > 0 ))
	then
		((indiceFichier++));
		nomFichierSortie="${oValeur}000${indiceFichier}"
	fi
fi

if (( t3Drapeau == 1 )) # strictement le même principe que pour -t 1
then
	./application/executable $commande -t 3 -i "$iValeur" -o "./fichiersGeneres/$nomFichierSortie"
	if (( $? != 0 )); then exit $?; fi
	((nombreFichiersAGenerer--))

	printf "Génération du diagramme \"./fichiersGeneres/$nomFichierSortie.png\" pour la température en mode 3...\n"
	if ! [ -x "./application/scriptsGnuplot/tm3.sh" ]
	then
		chmod u+x ./application/scriptsGnuplot/tm3.sh 2> /dev/null >> /dev/null
		if (( $? != 0 )); then erreurScriptGnuplot; fi
	fi
	./application/scriptsGnuplot/tm3.sh "./fichiersGeneres/$nomFichierSortie.dat" "./fichiersGeneres/$nomFichierSortie.png" 2> /dev/null >> /dev/null
	if (( $? != 0 )); then erreurScriptGnuplot; fi
	
	if (( nombreFichiersAGenerer > 0 ))
	then
		((indiceFichier++));
		nomFichierSortie="${oValeur}000${indiceFichier}"
	fi
fi

if (( p1Drapeau == 1 )) # strictement le même principe que pour -t 1
then
	./application/executable $commande -p 1 -i "$iValeur" -o "./fichiersGeneres/$nomFichierSortie"
	if (( $? != 0 )); then exit $?; fi
	((nombreFichiersAGenerer--))
	
	printf "Génération du diagramme \"./fichiersGeneres/$nomFichierSortie.png\" pour la pression en mode 1...\n"
	if ! [ -x "./application/scriptsGnuplot/pm1.sh" ]
	then
		chmod u+x ./application/scriptsGnuplot/pm1.sh 2> /dev/null >> /dev/null
		if (( $? != 0 )); then erreurScriptGnuplot; fi
	fi
	./application/scriptsGnuplot/pm1.sh "./fichiersGeneres/$nomFichierSortie.dat" "./fichiersGeneres/$nomFichierSortie.png" 2> /dev/null >> /dev/null
	if (( $? != 0 )); then erreurScriptGnuplot; fi

	if (( nombreFichiersAGenerer > 0 ))
	then
		((indiceFichier++));
		nomFichierSortie="${oValeur}000${indiceFichier}"
	fi
fi

if (( p2Drapeau == 1 )) # strictement le même principe que pour -t 1
then
	./application/executable $commande -p 2 -i "$iValeur" -o "./fichiersGeneres/$nomFichierSortie"
	if (( $? != 0 )); then exit $?; fi
	((nombreFichiersAGenerer--))
	
	printf "Génération du diagramme \"./fichiersGeneres/$nomFichierSortie.png\" pour la pression en mode 2...\n"
	if ! [ -x "./application/scriptsGnuplot/pm2.sh" ]
	then
		chmod u+x ./application/scriptsGnuplot/pm2.sh 2> /dev/null >> /dev/null
		if (( $? != 0 )); then erreurScriptGnuplot; fi
	fi
	./application/scriptsGnuplot/pm2.sh "./fichiersGeneres/$nomFichierSortie.dat" "./fichiersGeneres/$nomFichierSortie.png" 2> /dev/null >> /dev/null
	if (( $? != 0 )); then erreurScriptGnuplot; fi

	if (( nombreFichiersAGenerer > 0 ))
	then
		((indiceFichier++));
		nomFichierSortie="${oValeur}000${indiceFichier}"
	fi
fi

if (( p3Drapeau == 1 )) # strictement le même principe que pour -t 1
then
	./application/executable $commande -p 3 -i "$iValeur" -o "./fichiersGeneres/$nomFichierSortie"
	if (( $? != 0 )); then exit $?; fi
	((nombreFichiersAGenerer--))

	printf "Génération du diagramme \"./fichiersGeneres/$nomFichierSortie.png\" pour la pression en mode 3...\n"
	if ! [ -x "./application/scriptsGnuplot/pm3.sh" ]
	then
		chmod u+x ./application/scriptsGnuplot/pm3.sh 2> /dev/null >> /dev/null
		if (( $? != 0 )); then erreurScriptGnuplot; fi
	fi
	./application/scriptsGnuplot/pm3.sh "./fichiersGeneres/$nomFichierSortie.dat" "./fichiersGeneres/$nomFichierSortie.png" 2> /dev/null >> /dev/null
	if (( $? != 0 )); then erreurScriptGnuplot; fi
	
	if (( nombreFichiersAGenerer > 0 ))
	then
		((indiceFichier++));
		nomFichierSortie="${oValeur}000${indiceFichier}"
	fi
fi

if (( wDrapeau == 1 )) # strictement le même principe que pour -t 1
then
	./application/executable $commande -w -i "$iValeur" -o "./fichiersGeneres/$nomFichierSortie"
	if (( $? != 0 )); then exit $?; fi
	((nombreFichiersAGenerer--))

	printf "Génération du diagramme \"./fichiersGeneres/$nomFichierSortie.png\" pour le vent...\n"
	if ! [ -x "./application/scriptsGnuplot/vent.sh" ]
	then
		chmod u+x ./application/scriptsGnuplot/vent.sh 2> /dev/null >> /dev/null
		if (( $? != 0 )); then erreurScriptGnuplot; fi
	fi
	./application/scriptsGnuplot/vent.sh "./fichiersGeneres/$nomFichierSortie.dat" "./fichiersGeneres/$nomFichierSortie.png" 2> /dev/null >> /dev/null
	if (( $? != 0 )); then erreurScriptGnuplot; fi

	if (( nombreFichiersAGenerer > 0 ))
	then
		((indiceFichier++));
		nomFichierSortie="${oValeur}000${indiceFichier}"
	fi
fi

if (( hDrapeau == 1 )) # strictement le même principe que pour -t 1
then
	./application/executable $commande -h -i "$iValeur" -o "./fichiersGeneres/$nomFichierSortie"
	if (( $? != 0 )); then exit $?; fi
	((nombreFichiersAGenerer--))

	printf "Génération du diagramme \"./fichiersGeneres/$nomFichierSortie.png\" pour l'altitude...\n"
	if ! [ -x "./application/scriptsGnuplot/hauteur.sh" ]
	then
		chmod u+x ./application/scriptsGnuplot/hauteur.sh 2> /dev/null >> /dev/null
		if (( $? != 0 )); then erreurScriptGnuplot; fi
	fi
	./application/scriptsGnuplot/hauteur.sh "./fichiersGeneres/$nomFichierSortie.dat" "./fichiersGeneres/$nomFichierSortie.png" 2> /dev/null >> /dev/null
	if (( $? != 0 )); then erreurScriptGnuplot; fi

	if (( nombreFichiersAGenerer > 0 ))
	then
		((indiceFichier++));
		nomFichierSortie="${oValeur}000${indiceFichier}"
	fi
fi

if (( mDrapeau == 1 )) # strictement le même principe que pour -t 1
then
	./application/executable $commande -m -i "$iValeur" -o "./fichiersGeneres/$nomFichierSortie"
	if (( $? != 0 )); then exit $?; fi
	((nombreFichiersAGenerer--))

	printf "Génération du diagramme \"./fichiersGeneres/$nomFichierSortie.png\" pour l'humidité...\n"
	if ! [ -x "./application/scriptsGnuplot/humidite.sh" ]
	then
		chmod u+x ./application/scriptsGnuplot/humidite.sh 2> /dev/null >> /dev/null
		if (( $? != 0 )); then erreurScriptGnuplot; fi
	fi
	./application/scriptsGnuplot/humidite.sh "./fichiersGeneres/$nomFichierSortie.dat" "./fichiersGeneres/$nomFichierSortie.png" 2> /dev/null >> /dev/null
	if (( $? != 0 )); then erreurScriptGnuplot; fi

	if (( nombreFichiersAGenerer > 0 ))
	then
		((indiceFichier++));
		nomFichierSortie="${oValeur}000${indiceFichier}"
	fi
fi

exit 0 # si on en est là c'est que tout s'est bien passé donc on retourne 0