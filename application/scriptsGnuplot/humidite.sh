#!/bin/bash

if [ $# -ne 2 ] # on regarde qu'il y a bien 2 arguments en paramètres
then # sinon erreur
	printf "\n-!- erreur interne -!-\n"
	exit 1
fi

## Ce bloc balise les commandes gnuplot ##

# ce script est équivalent à celui des hauteurs
gnuplot << EOF
	
	set datafile separator ";"
	set terminal png size 14400,7200 font ",50"
	set output "$2"
	unset key

	set size 0.95, 0.95
	set origin 0.025, 0.025
	set title "Diagramme des Humidités Maximales" font ",100"
	set grid xtic ls 8
	set grid ytic ls 8
	set xtics 5
	set ytics 5

	set xlabel "Longitude" font ",100" offset 0,-1.75,0
	set ylabel "Latitude" font ",100" offset -0.75
	set autoscale xfix
	set autoscale yfix
	set border linewidth 8

	set pm3d map
	set dgrid3d 500,500,2
	set pm3d interpolate 2,2

	splot "$1" u 2:1:3 with pm3d

EOF

exit $? # on retourne ce qui c'est passé pour la dernière commande (si ok ou non)