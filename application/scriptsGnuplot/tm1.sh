#!/bin/bash

if [ $# -ne 2 ] # on regarde qu'il y a bien 2 arguments en paramètres
then # sinon erreur
	printf "\n-!- erreur interne -!-\n"
	exit 1
fi

# ce script est équivalent à celui des pressions en mode 1

taille=$(((`wc -l $1 | cut -d" " -f 1`)*51))

if (( $taille < 1920 ))
then
	taille=1920
fi

cat -n $1 | sed -e '/#/d' -e 's/    //g' -e 's/ //g' -e 's/\t/;/g' > ./application/scriptsGnuplot/temporaire.dat 2> /dev/null
if (( $? != 0 )); then exit $?; fi
chmod u+r ./application/scriptsGnuplot/temporaire.dat 2> /dev/null >> /dev/null
if (( $? != 0 )); then exit $?; fi

## Ce bloc balise les commandes gnuplot ##

gnuplot << EOF

	set datafile separator ";"
	set terminal png size $taille,1080
	set output "$2"
	set title "Diagramme des Températures en Mode 1"
	set xlabel "ID de Station" offset 0,-3.5,0
	set ylabel "Température"
	set grid
	set border 3
	set xtics nomirror
	set ytics nomirror
	set xtics rotate by 45 offset -1.5,-3,0

	set size 0.95,0.95
	set origin 0.025,0.025

	set style fill transparent solid 0.5 noborder

	plot './application/scriptsGnuplot/temporaire.dat' using 1:3:4:xtic(2) w filledcurve fc rgb '#FFE94A' title 'Plage Minimum - Maximum', '' using 1:5 lt rgb 'red' w l title 'Températures Moyennes'

EOF

rm -f ./application/scriptsGnuplot/temporaire.dat

exit $? # on retourne ce qui c'est passé pour la dernière commande (si ok ou non)