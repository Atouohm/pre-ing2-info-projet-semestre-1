#!/bin/bash

if [ $# -ne 2 ] # on regarde qu'il y a bien 2 arguments en paramètres
then # sinon erreur
	printf "\n-!- erreur interne -!-\n"
	exit 1
fi

taille=$(((`wc -l $1 | cut -d" " -f 1`)*51))

if (( $taille < 1920 ))
then
	taille=1920
fi

cat -n $1 | sed -e '/#/d' -e 's/    //g' -e 's/ //g' -e 's/\t/;/g' > ./application/scriptsGnuplot/temporaire.dat 2> /dev/null # on enlève les lignes de commentaires tout en rajoutant en tant que première colonne le numéro de la colonne
if (( $? != 0 )); then exit $?; fi # on vérifie que tout s'est bien passé
chmod u+r ./application/scriptsGnuplot/temporaire.dat 2> /dev/null >> /dev/null # on met les droits pour lire le fichier créé juste avant
if (( $? != 0 )); then exit $?; fi # on vérifie encore que tout s'est bien passé

## Ce bloc balise les commandes gnuplot ##

gnuplot << EOF

	set datafile separator ";" # le séparateur est le point virgule
	set terminal png size $taille,1080 # la sortie une image png
	set output "$2" # on donne le bon nom au fichier de sortie
	set title "Diagramme des Pressions en Mode 1" # on met un jolie titre
	set xlabel "ID de Station" offset 0,-3.5,0 # on donne un nom à l'axe des x
	set ylabel "Pression" # pareil pour l'axe des y
	set grid # on met une grille pour facilement se repérer en lisant le graphique
	set border 3 # on définie les bordures par le style prédéfinie 3
	set xtics nomirror # on met pas de tics haut
	set ytics nomirror # ni à droite
	set xtics rotate by 45 offset -1.5,-3,0 # pour le tics de l'axe x, on les tourne un peu

	set size 0.95,0.95 # on dezoome un tout petit peu le graphique
	set origin 0.025,0.025 # et on le re-centre

	set style fill transparent solid 0.5 noborder # on définit un nouveau style de remplissage

	plot './application/scriptsGnuplot/temporaire.dat' using 1:3:4:xtic(2) w filledcurve fc rgb '#4AC8FF' title 'Plage Minimum - Maximum', '' using 1:5 lt rgb 'red' w l title 'Pressions Moyennes' # et on trace, sachant que les indices (de 1 en 1) en colonne 1 permettent de tracer un graphique où les tics en x sont les id de station séparées de façon équidistante à chaque fois

EOF

rm -f ./application/scriptsGnuplot/temporaire.dat 2> /dev/null >> /dev/null # on oublie pas de supprimer le fichier temporaire

exit $? # on retourne ce qui c'est passé pour la dernière commande (si ok ou non)