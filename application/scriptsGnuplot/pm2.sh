#!/bin/bash

if [ $# -ne 2 ] # on regarde qu'il y a bien 2 arguments en paramètres
then # sinon erreur
	printf "\n-!- erreur interne -!-\n"
	exit 1
fi

# le but ce de début de script est de permettre un affichage des données qui s'adapte un maximum à ces dernières
dateDebut="`cat $1 | head -2 | tail -1 | cut -d ';' -f1`" # on récupère la date de début
dateFin="`cat $1 | tail -1 | cut -d ';' -f1`" # puis la date de fin
nombreJours=$(( (`date --date="$dateFin" +"%s"` - `date --date="$dateDebut" +"%s"`)/(3600*24) )) # pour calculer le nombre de jours entre les 2

taille=$(( 50*($nombreJours/365)**2 + 1920 )) # on détermine la longueur de l'image grâce au nombre de jours et un polynome du second degrès (donc une courbe) qui a été créé pour donner une taille adaptée au nombre de jours et à ce script
graduations=$(( 24*30 )) # on détermine les graduation en x
formatSortie="%b %d %Y" # ainsi que le format de sortie pour les tics en x

if (( $nombreJours <= 2 )) # mais si le nombre de jours et inférieur à 2
then # alors on "zoom" en adaptant la graduation et en affichant l'heure
	graduations=1
	formatSortie="%b %d %Y %H"
elif (( $nombreJours <= 60 )) # sinon si c'est <= à 60 (et > 2 ducoup)
then # on adapte aussi
	graduations=24
elif (( $nombreJours <= 365 )) # pareil si le nombre de jours est inférieur au égal à 365
then
	graduations=$(( 24*7 ))
fi

## Ce bloc balise les commandes gnuplot ##

gnuplot << EOF

	set datafile separator ";" # le séparateur est le point virgule
	set terminal png size $taille,1080 # la sortie une image png
	set output "$2" # on donne le bon nom au fichier de sortie
	set title "Diagramme des Pressions en Mode 2" # on met un jolie titre
	set xlabel "Date" offset 0,-7,0 # on donne un nom à l'axe des x
	set ylabel "Pression" # pareil pour l'axe des y
	set grid # on met une grille pour facilement se repérer en lisant le graphique
	set border 3 # on définie les bordures par le style prédéfinie 3
	set xtics nomirror # on met pas de tics haut
	set ytics nomirror # ni à droite
	set xtics rotate by 45 offset -3.5,-6 # pour le tics de l'axe x, on les tourne un peu

	set xdata time # on dit que les données en x sont temporelles
	set timefmt "%Y-%m-%dT%H:%M:%SZ" # on donne le format d'entrée
	set xtics format "$formatSortie" # et on donne le format de sortie qu'on a déterminé avant

	set xrange ["$dateDebut":"$dateFin"] # on donne la fin et le début pour l'axe x
	set xtics 3600*$graduations # ainsi que les tics à afficher

	set size 0.95,0.95 # on dezoom un peu
	set origin 0.025,0.025 # et on le re-centre

	plot '$1' using 1:2 lt rgb 'blue' w l title 'Pressions' # et enfin on trace la courbe

EOF

exit $? # on retourne ce qui c'est passé pour la dernière commande (si ok ou non)