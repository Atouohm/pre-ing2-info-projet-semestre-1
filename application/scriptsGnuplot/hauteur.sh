#!/bin/bash

if [ $# -ne 2 ] # on regarde qu'il y a bien 2 arguments en paramètres
then # sinon erreur
	printf "\n-!- erreur interne -!-\n"
	exit 1
fi

## Ce bloc balise les commandes gnuplot ##

gnuplot << EOF

	set datafile separator ";" # on dit que le séparateur est le point virgule
	set terminal png size 14400,7200 font ",50" # on paramètre la sortie comme étant une image png avec une certaine taille et police d'écriture
	set output "$2" # on paramètre le nom du fichier de sortie
	unset key # on enlève tout paramètre de légende prédéfinie

	# on gère la taille de l'image, le titre, le style de la grille ainsi que l'intervalle des tics pour x et y
	set size 0.95, 0.95
	set origin 0.025, 0.025
	set title "Diagramme des Altitudes" font ",100"
	set grid xtic ls 8
	set grid ytic ls 8
	set xtics 5
	set ytics 5

	# on donne un nom à nos axe, on dit de place le min et le max pour chacun d'eux automatiquement, et on définit l'épaisseur des axes à 8
	set xlabel "Longitude" font ",100" offset 0,-1.75,0
	set ylabel "Latitude" font ",100" offset -0.75
	set autoscale xfix
	set autoscale yfix
	set border linewidth 8

	set pm3d map # on créer un graphique 3d mais vu du dessus
	set dgrid3d 500,500,2 # on initialise une grille car nos valeurs sont éparpillée et nous on veut des données placées sous forme de grille
	set pm3d interpolate 2,2 # on interpolle un peu pour faire jolie

	splot "$1" u 2:1:3 with pm3d # on trace la surface (3d mais vu du dessus)

EOF

exit $? # on retourne ce qui c'est passé pour la dernière commande (si ok ou non)