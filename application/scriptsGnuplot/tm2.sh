#!/bin/bash

if [ $# -ne 2 ] # on regarde qu'il y a bien 2 arguments en paramètres
then # sinon erreur
	printf "\n-!- erreur interne -!-\n"
	exit 1
fi

# ce script est équivalent à celui des pressions en mode 2

dateDebut="`cat $1 | head -2 | tail -1 | cut -d ';' -f1`"
dateFin="`cat $1 | tail -1 | cut -d ';' -f1`"
nombreJours=$(( (`date --date="$dateFin" +"%s"` - `date --date="$dateDebut" +"%s"`)/(3600*24) ))

taille=$(( 50*($nombreJours/365)**2 + 1920 ))
graduations=$(( 24*30 ))
formatSortie="%b %d %Y"

if (( $nombreJours <= 2 ))
then
	graduations=1
	formatSortie="%b %d %Y %H"
elif (( $nombreJours <= 60 ))
then
	graduations=24
elif (( $nombreJours <= 365 ))
then
	graduations=$(( 24*7 ))
fi

## Ce bloc balise les commandes gnuplot ##

gnuplot << EOF

	set datafile separator ";"
	set terminal png size $taille,1080
	set output "$2"
	set title "Diagramme des Températures en Mode 2"
	set xlabel "Date" offset 0,-7,0
	set ylabel "Température"
	set grid
	set border 3
	set xtics nomirror
	set ytics nomirror
	set xtics rotate by 45 offset -3.5,-6

	set xdata time
	set timefmt "%Y-%m-%dT%H:%M:%SZ"
	set xtics format "$formatSortie"

	set xrange ["$dateDebut":"$dateFin"]
	set xtics 3600*$graduations

	set size 0.95,0.95
	set origin 0.025,0.025

	plot '$1' using 1:2 lt rgb 'red' w l title 'Températures'

EOF

exit $? # on retourne ce qui c'est passé pour la dernière commande (si ok ou non)