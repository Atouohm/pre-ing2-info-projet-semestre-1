#!/bin/bash

# ce début qui permet d'adapter la taille de l'image générée aux données en entrée est le même que pour pm2

dateDebut="`cat $1 | head -2 | tail -1 | cut -d ';' -f1`"
dateFin="`cat $1 | tail -1 | cut -d ';' -f1`"
nombreJours=$(( (`date --date="$dateFin" +"%s"` - `date --date="$dateDebut" +"%s"`)/(3600*24) ))
taille=$(( 50*($nombreJours/365)**2 + 1920 ))
graduations=$(( 24*30 ))
formatSortie="%b %d %Y"

if (( $nombreJours <= 60 ))
then
	graduations=24
elif (( $nombreJours <= 365 ))
then
	graduations=$(( 24*7 ))
fi

tail -n +2 $1 | cut -d ';' -f2 | sort | uniq > ./application/scriptsGnuplot/heuresUniques.txt 2> /dev/null # on récupère les heures des mesures dans un fichier

while read heure # on parcours toutes les heures
do # et pour chaque heure on récupère tous les enregistrements faits à cette heure, puis on trie par id de station, pour au final tout stocker dans un fichier temporaire (1 différent pour chaque heure) tout en séparant d'une ligne vide quand on change d'id de station (de façon à ce que gnuplot trace des courbes différentes pour chaque station)
	tail -n +2 $1 | awk -F ";" -v h=$heure '$2==h {print $1 " " $3 " " $4}' | sort -k3,3 | awk 'BEGIN{dernierIdStation="";} {if(dernierIdStation!=$3) {dernierIdStation=$3; printf("\n")}; print}' > ./application/scriptsGnuplot/temp$heure.dat 2> /dev/null
done < ./application/scriptsGnuplot/heuresUniques.txt # on met en entrée du read (du while un peu plus haut) les heures (sachant que dans le fichier les heures sont à la ligne pour simuler des entrée utilisateur)

adresseScript="./application/scriptsGnuplot/script.gnu" # on met l'adresse du script gnuplot qu'on va créer dans une variable pour simplifier la suite

echo "set term png size $taille,1080" > $adresseScript 2> /dev/null # on définit la taille et on dit que ça sera un png
echo "set xdata time" >> $adresseScript 2> /dev/null # on dit que les données en x sont des dates
echo "set timefmt '%Y-%m-%d'" >> $adresseScript 2> /dev/null # on donne le format
echo "set xlabel 'Date' offset 0,-7,0" >> $adresseScript 2> /dev/null # on met le titre à l'axe des x
echo "set ylabel 'Température'" >> $adresseScript 2> /dev/null # pareil pour selui des y
echo "set title 'Diagramme des Températures en Mode 3'" >> $adresseScript 2> /dev/null # on met le titre du graphique
echo "set grid" >> $adresseScript 2> /dev/null # on met un grille
echo "set output '$2'" >> $adresseScript 2> /dev/null # on définit le nom du fichier de sortie
echo "set xtics format '$formatSortie'" >> $adresseScript 2> /dev/null # on définit le format des tics en x
echo "set autoscale xfixmin" >> $adresseScript 2> /dev/null # on fait en sorte que le graphique commence à la plus petite date
echo "set autoscale xfixmax" >> $adresseScript 2> /dev/null # et se termine à la plus grande
echo "set xtics 3600*$graduations" >> $adresseScript 2> /dev/null # on définit les tics de l'axe des x
echo "set size 0.95,0.95" >> $adresseScript 2> /dev/null # la taille du graphique un peu réduit
echo "set origin 0.025,0.025" >> $adresseScript 2> /dev/null # on le re-centre
echo "set xtics nomirror" >> $adresseScript 2> /dev/null # par d'axe en haut
echo "set ytics nomirror" >> $adresseScript 2> /dev/null # ni en bas
echo "set xtics rotate by 45 offset -3.5,-6" >> $adresseScript 2> /dev/null # on tourne un peu les tics de l'axe des x
echo "set key outside" >> $adresseScript 2> /dev/null # on met la légende dehors pour être sur qu'elle ne chevauche pas une courbe
echo -n "plot " >> $adresseScript 2> /dev/null # on prépare la commande plot

# on définit au maximum 23 codes html de couleurs différentes
couleur1="#ff0000";
couleur2="#00ff00";
couleur3="#0000ff";
couleur4="#ffff00";
couleur5="#ff00ff";
couleur6="#00ffff";
couleur7="#ffa500";
couleur8="#800080";
couleur9="#800000";
couleur10="#008080";
couleur11="#a52a2a";
couleur12="#8b0000";
couleur13="#008b8b";
couleur14="#006400";
couleur15="#8b008b";
couleur16="#ffc0cb";
couleur17="#4b0082";
couleur18="#8a2be2";
couleur19="#daa520";
couleur20="#ff8c00";
couleur21="#6a5acd";
couleur22="#2f4f4f";
couleur23="#9400d3";
i=0; # pour suivre le nombre d'heure que l'on a fait
while read heure
do
	i=$(($i+1)) # on incrémente de 1, on pourrait aussi écrire let i++
	couleur="couleur$i" # on prend la ième couleur
	echo -n "'./application/scriptsGnuplot/temp$heure.dat' using 1:2 with lines lt rgb '${!couleur}' title 'Heure: ${heure}h00', " >> $adresseScript 2> /dev/null # et on ajoute au script le tracé du fichier .dat associé à l'heure actuellement traitée
done < ./application/scriptsGnuplot/heuresUniques.txt # pareil que le while plus haut

gnuplot $adresseScript 2> /dev/null >> /dev/null # on lance le script

# on supprime tous les fichhiers temporaires
while read heure
do
	rm -f ./application/scriptsGnuplot/temp${heure}.dat 2> /dev/null >> /dev/null
done < ./application/scriptsGnuplot/heuresUniques.txt 2> /dev/null >> /dev/null

rm -f ./application/scriptsGnuplot/heuresUniques.txt 2> /dev/null >> /dev/null

rm -f $adresseScript 2> /dev/null >> /dev/null

exit $? # on retourne ce qui c'est passé pour la dernière commande (si ok ou non)