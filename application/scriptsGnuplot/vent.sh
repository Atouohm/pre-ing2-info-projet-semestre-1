#!/bin/bash

if [ $# -ne 2 ] # on regarde qu'il y a bien 2 arguments en paramètres
then # sinon erreur
	printf "\n-!- erreur interne -!-\n"
	exit 1
fi

## Ce bloc balise les commandes gnuplot ##

gnuplot << EOF

	set datafile separator ";" # le séparateur est le point virgule
	set terminal png size 14400,7200 font ",50" # la sortie une image png
	set output "$2" # on donne le bon nom au fichier de sortie
	unset key # on enlève tout paramètre de légende prédéfinie

	set size 0.95, 0.95 # on dezoom un peu
	set origin 0.025, 0.025 # et on le re-centre
	set title "Diagramme des Vents Moyens" font ",100"
	set grid xtic ls 8 # on met des lignes verticales qui représentes les tics en x
	set grid ytic ls 8 # on met des lignes hozirontales qui représentes les tics en y
	set xtics 5 # on choisit tout les combiens on affiche un tic en x
	set ytics 5 # pareil pour y
	stats "$1" using 3 name "x" nooutput # on récupère les stats de la colonne 3 (ie x), comme le min et le max
	stats "$1" using 2 name "y" nooutput # pareil pour la 2 (ie y)

	# pour x et y on définit le min et max de façon à s'écarte un peux de valeurs extrême et de-zoomer un peu
	x_min=x_min-10
	x_max=x_max+10
	y_min=y_min-10
	y_max=y_max+10

	# on définit les amplitudes pour les 2 axes ainsi que leur nom et les bordures
	set xrange [x_min:x_max]
	set yrange [y_min:y_max]
	set xlabel "Longitude" font ",100" offset 0,-1.75,0
	set ylabel "Latitude" font ",100" offset -0.75
	set border linewidth 8

	set size ratio (y_max-y_min)/(x_max-x_min) # on garde un ration 1/2 suivant les amplitudes
	plot "$1" using 3:2:(\$5):(-\$4+90) with arrows linewidth 4 linecolor 'red' # puis on trace les vecteurs

EOF

exit $? # on retourne ce qui c'est passé pour la dernière commande (si ok ou non)