/* inclusion des headers des bibliothèques basiques */
#include "librairiesBasiques.h"

/* inclusion de nos headers pour nos bibliothèques */
#include "commandes.h"
#include "generateurs.h"
#include "utilitaires.h"

/* fonction main prenant en paramètre les options et arguments de la commande d'exécution. Tout est défini comme constant pour ne pas que cela soit modifié */
int main(const int argc, const char* const argv[]){
	LISTEINSTRUCTIONS* listeInstructions;

	if(recupererInstructions(argc, argv, &listeInstructions)){ // on récupère les instructions de la commande et on regarde si elle est valide (si oui on rentre dans ce if)
		char* fichierSortie = malloc(128*sizeof(char)); // on alloue la chaîne de l'adresse du fichier de sortie
		strcpy(fichierSortie, "./meteo.dat"); // et on lui met la valeur par défaut
		int retour = 3; // on déclare et initialiser la valeur de retour à 3 (cas erreur interne car elle n'est pas censée rester à 3)
		FILTRE* filtre = initialiserFiltre(); // on déclare, alloue et initialiser le filtre aux valeurs par défaut (0)

		if(listeInstructions->aideDrapeau){ // si il y avait --help on affiche l'aide et on quite
			aideApplication();
			retour = EXIT_SUCCESS;
		} else{ // sinon on analyse toutes les instructions

			if(listeInstructions->oDrapeau){ // si un fichier de sortie est précisé
				strcpy(fichierSortie, listeInstructions->oValeur); // on met à jour la chaîne fichierSortie
				strcat(fichierSortie, ".dat"); // on rajoute l'extension pour signifier que c'est un fichier de données
			}

			if(listeInstructions->gDrapeau){ // si -g on met à jour le filtre
				filtre->longitude.longitudeMin = listeInstructions->gValeurMin;
				filtre->longitude.longitudeMax = listeInstructions->gValeurMax;
				filtre->longitude.actif = 1;
			}

			if(listeInstructions->aDrapeau){ // si -a on met à jour le filtre
				filtre->latitude.latitudeMin = listeInstructions->aValeurMin;
				filtre->latitude.latitudeMax = listeInstructions->aValeurMax;
				filtre->latitude.actif = 1;
			}

			if(listeInstructions->dDrapeau){ // si -d on met à jour le filtre
				filtre->date.dateMin.annee = listeInstructions->dValeurMin.annee;
				filtre->date.dateMin.mois = listeInstructions->dValeurMin.mois;
				filtre->date.dateMin.jour = listeInstructions->dValeurMin.jour;

				filtre->date.dateMax.annee = listeInstructions->dValeurMax.annee;
				filtre->date.dateMax.mois = listeInstructions->dValeurMax.mois;
				filtre->date.dateMax.jour = listeInstructions->dValeurMax.jour;

				filtre->date.actif = 1;
			}

			if(listeInstructions->tDrapeau){ // si -t
				printf("\nGénération du fichier de données \"%s\" pour la température en mode %d...\n", fichierSortie, listeInstructions->tValeur);
				retour = temperature(listeInstructions->tValeur, *filtre, listeInstructions->iValeur, fichierSortie); // on génère le fichier de données et on récupère si ça s'est bien passé ou non
			} else if(listeInstructions->pDrapeau){ // sinon si -p
				printf("\nGénération du fichier de données \"%s\" pour la pression en mode %d...\n", fichierSortie, listeInstructions->pValeur);
				retour = pression(listeInstructions->pValeur, *filtre, listeInstructions->iValeur, fichierSortie);
			} else if(listeInstructions->wDrapeau){ // sinon si -w
				printf("\nGénération du fichier de données \"%s\" pour le vent...\n", fichierSortie);
				retour = vent(*filtre, listeInstructions->iValeur, fichierSortie);
			} else if(listeInstructions->hDrapeau){ // sinon si -h
				printf("\nGénération du fichier de données \"%s\" pour la hauteur...\n", fichierSortie);
				retour = hauteur(*filtre, listeInstructions->iValeur, fichierSortie);
			} else if(listeInstructions->mDrapeau){ // sinon si -m
				printf("\nGénération du fichier de données \"%s\" pour l'humidité...\n", fichierSortie);
				retour = humidite(*filtre, listeInstructions->iValeur, fichierSortie);
			}
		}

		/* on libère tout ce qui a été malloc*/
		free(listeInstructions->iValeur);
		free(listeInstructions->oValeur);
		free(listeInstructions);
		free(fichierSortie);
		free(filtre);
		if(retour==3){printf("\n-!- erreur interne -!-\n");} // si le retour est à 3 c pas bon
		exit(retour); // sinon on retourne la valeur de retour (0 si ok, 1 si pas ok)
	} else{ // si elle ne l'était pas alors erreur
		printf("\n-!- erreur dans la commande -!-\n");
		aideApplication();
		exit(EXIT_FAILURE);
	}
}
