PRE-ING2 Projet Décembre-Janvier 2022/2023, groupe "ALTF4"

Membres :
Julien GITTON (G1)
Matheo BIOUDI (G1)
Tara FAZILLE (G1)
Dorian BARRERE (G1)

Les dépendances :
- Commandes : time (version GNU) ; chmod ; cd ; make ; printf ; echo ; gcc ; rm ; gnuplot ; cat ; sed
- Librairies C : stdio.h ; stdlib.h ; string.h ; math.h ; dirent.h

Informations :
1) L'application doit être lancée grâce au fichier ./application.sh. Il est néanmoins possible d'utiliser directement l'exécutable produit par le C afin de lever la restriction et d'être beaucoup plus flexible sur certaines choses, à condition de bien le manipuler car l'application n'a pas été conçu pour cela.
2) Lancée l'application doit se faire uniquement lorsque le dossier courant est celui de l'application (où se trouve le fichier ./application.sh).
3) --help pour afficher l'aide
4) Les fichiers générés sont par défaut dans le dossier ./fichiersGeneres.
5) Le fichier de données doit être lisible et formaté en csv de façon suivant :
	- ; comme seul et unique délimiteur
	- Les colonnes doivent être selon cet ordre :
		1) ID OMM station (entier)
		2) Date (ISO 8601)
		3) Pression au niveau de la mer (Pa)
		4) Direction du vent moyen 10 mn (degré)
		5) Vitesse du vent moyen 10 mn (m/s)
		6) Humidité (%)
		7) Pression au niveau de la station (Pa)
		8) Variation de pression en 24 heures (Pa)
		9) Précipitations dans les 24 dernières heures (mm)
		10) Coordonnées (<latitude décimale>, <longitude décimale>)
		11) Température (°C)
		12) Température minimale sur 24 heures (°C)
		13) Température maximale sur 24 heures (°C)
		14) Altitude (m)
		15) Commune (entier)
6) La première ligne du csv n'est pas lue étant destinée aux noms des colonnes.
7) Le mode 3 est particulièrement lent par rapport aux autres car chaque enregistrement d'entrée est un enregistrement de sortie. De plus, le graphique associé n'est pas adapté sa définition même à un grand nombre de stations différentes associé à des mesures effectuées toutes les heures (le graphique sera généré mais ne sera que peu lisible).
8) Il est possible de personnaliser les graphiques générés en modifiant les scripts gnuplot présents dans ./application/scriptsGnuplot.
9) Les dates des mesures doivent être au moins de 1970.
10) Le temps d'exécution moyen pour une option exclusive de type générateur (-t, -p, -w, -h, -m) est de 15-16 secondes sur un ordinateur moyen (avec le fichier data.csv fourni en exemple) :
	-t 1 = 12s
	-t 2 = 12s
	-t 3 = 30s
	-p 1 = 10s
	-p 2 = 10s
	-p 3 = 27s
	-w = 11s
	-h = 14s
	-m = 13s
11) La commande pour générer tous les fichiers montrés dans l'exemple est : ./application.sh -i "fichiersExemples/data.csv" -t 1 -t 2 -t 3 -p 1 -p 2 -p 3 -w -h -m.
12) Le script de lancement ./application.sh compile automatiquement le code c en un exécutable si celui-ci est absent (en appelant la règle "exe" du ./application/makefile).
13) Si nécessaire il est possible de nettoyer les fichiers objets servant à la compilation en appelant la règle "nettoyer" du makefile située dans le répertoire de "application".

Les options génériques :
- --help : affiche l'aide détaillée
- -i <nom_fichier>.csv : permet de spécifier le chemin du fichier CSV d’entrée (son adresse depuis le dossier courant). Cette option est OBLIGATOIRE
- -o <nom_fichier> : permet de donner un nom au fichier de sortie contenant les données (lors de l'appel de application.sh il se génère par défaut dans le répertoire fichiersGeneres, mais lors de l'appel direct de l'exécutable il est généré par défaut directement dans le dossier courant). ATTENTION L'extension .dat sera automatiquement rajoutée

Les options de restrictions sur les données :
- -g <min> <max> : permet de ne conserver que les données qui sont dans l’intervalle de longitudes [<min>..<max>]
- -a <min> <max> : permet de ne conserver que les données qui sont dans l’intervalle de latitudes [<min>..<max>]
- -d <min> <max> : permet de ne conserver que les données qui sont dans l’intervalle de dates [<min>..<max>]. Le format des dates est YYYY-MM-DD (année-mois-jour)
	ATTENTION Ces 3 dernières options -g, -a et -d sont optionnelles.
	ATTENTION Les dates des mesures doivent être plus récentes que 1 Janvier 1970.
	ATTENTION Les options suivantes -F, -G, -S, -A, -O et -Q sont exclusives et ne sont pas compatibles avec -g et -a.
- -F : limite les mesures à celles présentes en France métropolitaine et la Corse
- -G : limite les mesures à celles présentes en Guyane
- -S : limite les mesures à celles présentes sur l'île Saint-Pierre et Miquelon
- -A : limite les mesures à celles présentes aux Antilles
- -O : limite les mesures à celles présentes dans l’Océan Indien
- -Q : limite les mesures à celles présentes en Antarctique

Les options de générations :
- -t <mode> : créer le diagramme des températures
- -p <mode> : créer le diagramme de pressions atmosphériques
	ATTENTION Pour les 2 options précédentes, il faut OBLIGATOIREMENT indiquer un <mode> :
		1 = type “barres d’erreur”
		2 = type “ligne simple”
		3 = type “multi-lignes”
- -w : créer le diagramme du vent de type “vecteurs”
- -h : créer le diagramme hauteur de type “carte interpolée et colorée”
- -m : créer le diagramme de l'humidité de type “carte interpolée et colorée”
	ATTENTION Au moins l'une des ces 5 dernières options doit être présente.
