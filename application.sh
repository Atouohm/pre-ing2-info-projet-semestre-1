#!/bin/bash

function erreurScript(){ # automatise la gestion d'une erreur dans la mise en place du script d'exécution
	printf "\n-!- erreur dans la mise en place du script d'exécution -!-\n"
	exit 1
}

function erreurCommande(){ # fonction qui automatique la gestion d'une erreur dans la commande (termine le script avec une erreur)
	./application/applicationCachee.sh erreurVoulue
	exit 1
}

if ! [ -d "./application" ] # on vérifie que le dossier de l'application est présent
then # si ce n'est pas le cas c'est pas bon
	echo "-!- erreur : le code source de l'application n'a pas été trouvé (veuillez lancer l'application depuis son propre dossier, où se trouve le fichier application.sh) -!-"
	exit 1
fi

if ! [ -e "./application/applicationCachee.sh" ] # on regarde si le script de l'application est présent
then # si ce n'est pas le cas c'est pas bon
	echo "erreur : le script de lancement de l'application est absent"
elif ! [ -x "./application/applicationCachee.sh" ] # sinon on vérifie que l'on peut l'exécuter
then # si ce n'est pas le cas on met les droits nécessaires
	chmod u+x ./application/applicationCachee.sh
	if (( $? != 0 )); then erreurScript; fi # mais si on n'avait pas le droit de le faire ou s'il y a une quelconque erreur on retourne une erreur
fi

## On récupère la commande pour directement la retransférer au script de l'application, seuleument, il faut traiter traiter le -i et -o séparément pour pouvoir prendre en compte les espaces dans les noms/adresses des fichiers ##

iValeur="" # argument du -i
oDrapeau=0 # savoir si le -o a été utilisé
oValeur="" # argument du -o
commande="" # commande contenant toutes les autres options
indiceArgument=1 # on commence la lectures des arguments à 1
while (( $indiceArgument <= $# )) # tant qu'on les a pas tous lus on continue
do
	case ${!indiceArgument} in
		"-i") # si c'est -i
			if (( $indiceArgument+1 > $# )); then erreurCommande; fi; # on vérifie qu'un argument suit (qu'il soit valide ou non, le c le vérifiera plus tard)
			((indiceArgument++)) # on va dessus
			iValeur="${!indiceArgument}";; # on le récupère
		"-o") # strictement identique à -i
			if (( $indiceArgument+1 > $# )); then erreurCommande; fi;
			oDrapeau=1
			((indiceArgument++))
			oValeur="${!indiceArgument}";;
		"--help") 
			./application/applicationCachee.sh --help
			exit $?;;
		*) commande="$commande ${!indiceArgument}" # sinon on ajoute l'argument présent à la suite de la commande
	esac
	((indiceArgument++)) # on avance de 1 dans la liste des arguments
done

## On appelle le script de l'application avec la commande (version GNU) time pour afficher le temps d'exécution de l'appel de l'application à la fin (il y a 2 petits cas suivant la présence du -o ou non) ##

if (( oDrapeau == 1 ))
then # s'il est présent on le prend en compte
	/usr/bin/time -q -f "\nLe script a duré %e seconde(s)." ./application/applicationCachee.sh $commande -i "$iValeur" -o "$oValeur"
else # sinon non
	/usr/bin/time -q -f "\nLe script a duré %e seconde(s)." ./application/applicationCachee.sh $commande -i "$iValeur"
fi

exit $? # on retourne ce que nous a retourné la commande time (qui retourne ce que retourne la commande passée en argument)